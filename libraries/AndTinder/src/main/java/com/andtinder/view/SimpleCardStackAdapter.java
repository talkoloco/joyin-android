package com.andtinder.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andtinder.R;
import com.andtinder.model.CardModel;

import pl.polidea.webimageview.WebImageView;

public final class SimpleCardStackAdapter extends CardStackAdapter {

	public SimpleCardStackAdapter(Context mContext) {
		super(mContext);
	}

	@Override
	public View getCardView(int position, CardModel model, View convertView, ViewGroup parent) {
		if(convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.std_card_inner, parent, false);
			assert convertView != null;
		}

        ((WebImageView) convertView.findViewById(R.id.userPic1)).setImageURLWithPlaceholder(model.getImage1(),R.drawable.xapng);
        ((WebImageView) convertView.findViewById(R.id.userPic2)).setImageURLWithPlaceholder(model.getImage2(),R.drawable.xapng);
        ((WebImageView) convertView.findViewById(R.id.userPic3)).setImageURLWithPlaceholder(model.getImage3(),R.drawable.xapng);
        ((WebImageView) convertView.findViewById(R.id.userPic4)).setImageURLWithPlaceholder(model.getImage4(),R.drawable.xapng);
	/*	((TextView) convertView.findViewById(R.id.title)).setText(model.getTitle());
		((TextView) convertView.findViewById(R.id.description)).setText(model.getDescription());*/

		return convertView;
	}
}
