CKEDITOR.plugins.addExternal( 'dnd-tile', '/cityofbostondnd/ck-plugins/dnd-tile/' );
CKEDITOR.plugins.addExternal( 'dnd-tile-title', '/cityofbostondnd/ck-plugins/dnd-tile-title/' );
CKEDITOR.plugins.addExternal( 'dnd-tile-with-image', '/cityofbostondnd/ck-plugins/dnd-tile-with-image/' );
CKEDITOR.plugins.addExternal( 'dnd-tile-tokens', '/cityofbostondnd/ck-plugins/dnd-tile-tokens/' );
Widget.prototype.initialContent = {

config: {

templates: [
'dnd-tile-title',
'dnd-tile',
'dnd-tile-with-image',
'dnd-button',
'dnd-tile-tokens'
],

toolbarName: 'dnd-tile',

initialContent: 'content/tile-builder-init-content.html'

},

init: function init(viewerEl) {
this.addIcons();
this.setContent(viewerEl);
},

addIcons: function addIcons() {
var extraPlugins = "," + this.config.templates.join()
Widget.prototype.CKconfig.extraPlugins = Widget.prototype.CKconfig.extraPlugins + extraPlugins;
Widget.prototype.CKconfig.toolbar = [
{ name: this.config.toolbarName, items: this.config.templates },
//			{ name:'saveCancel', items:['Save']},
{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
{ name:'links', items:['Link', 'Unlink'] },
{ name:'styles', items:['Font', 'FontSize'] },
{ name:'basicstyles', items:[ 'Bold', 'Italic', 'Underline', 'Strike', 'TextColor', 'BGColor' ] }
];
},

setContent: function setContent(viewerEl) {
var that = this;
$.ajax({
async: false,
url: this.getContentProxyUrl(this.config.initialContent),
success: function(data) {
that.writeContent(data, viewerEl);
}
});
},

getContentProxyUrl: function getProxyContentUrl(contentUrl) {
var startIndex = window.location.href.indexOf('url=') + 4;
var endIndex = window.location.href.indexOf('#');
var thisUrl = window.location.href.substring(startIndex, endIndex)
.replace(new RegExp('%3A', 'g'), ':')
.replace(new RegExp('%2F', 'g'), '/')
thisUrl = thisUrl.substring(0, thisUrl.lastIndexOf('/'))
return gadgets.io.getProxyUrl(thisUrl + '/' + contentUrl);
},

writeContent: function writeContent(data, viewerEl) {
var html = data;
viewerEl.html(html);
}

}
