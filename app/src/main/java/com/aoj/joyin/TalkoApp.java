package com.aoj.joyin;

import android.app.Application;
import android.location.Location;

import com.facebook.Session;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;
import com.aoj.joyin.apis.ApiConnector;
import com.aoj.joyin.model.TalkoUser;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Observable;


public class TalkoApp extends Application {

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    public final static String MEETUPS = "Meetups";
    public final static String CHATS = "Chats";
    public final static String TEST = "Test";


    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();


    private PersistentCookieStore asyncCookieStore ;

    /**
     * get observer by topic
     * @param  classType - topic.
     * @return observer object.
     */
    public Observable getObservable(Class<? extends Observable> classType) {
        if(observers.containsKey(classType)){
            return observers.get(classType);
        }
        try {


        Constructor<? extends Observable> constructor = classType.getConstructor();
        Observable instanceClass = constructor.newInstance();
        observers.put(classType,instanceClass);

        return this.getObservable(classType);
        } catch (Exception e) {

        }

        return null;

    }




    public  synchronized Tracker getTracker(TrackerName trackerName) {

        if (!mTrackers.containsKey(trackerName)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerName == TrackerName.APP_TRACKER) ? analytics.newTracker("UA-47489737-2")
                    : (trackerName == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.stats)
                    : analytics.newTracker(R.xml.stats);
            mTrackers.put(trackerName, t);

        }
        return mTrackers.get(trackerName);
    }

    public void setObservable(Observable observable) {
        this.observable = observable;
    }

    private Observable observable = new Observable();
    private AsyncHttpClient asyncHttpClient;
    private ApiConnector apiConnector;
    private Location userLocation;
    private String selectedTab;

    public HashMap<String, Object> getLocalCache() {
        return localCache;
    }
    public HashMap<Class, Observable> observers = new HashMap<Class, Observable>();

    public void setLocalCache(HashMap<String, Object> localCache) {
        this.localCache = localCache;
    }

    private HashMap<String,Object> localCache = new HashMap<String,Object>(100);

    public TalkoUser getUser() {

        return user;
    }

    public void setUser(TalkoUser user) {
        this.user = user;
    }

    private TalkoUser user;

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    private Session session;

    public void onCreate(){

        asyncCookieStore = new PersistentCookieStore(this.getApplicationContext());


    }
    public TalkoApp(){
        super();
    }

    public PersistentCookieStore getAsyncCookieStore() {
        return asyncCookieStore;
    }

    public void setAsyncCookieStore(PersistentCookieStore asyncCookieStore) {
        this.asyncCookieStore = asyncCookieStore;
    }

    public AsyncHttpClient getAsyncClient(){
        return asyncHttpClient;
    }

    public ApiConnector getApiConnector() {

        ApiConnector apiConnector = new ApiConnector();
        AsyncHttpClient asyncApiCall = new AsyncHttpClient();
        asyncApiCall.setCookieStore(this.getAsyncCookieStore());
        apiConnector.setContext(this);
        apiConnector.setClient(asyncApiCall);
        return apiConnector;

    }

    public void setApiConnector(ApiConnector apiConnector) {
        this.apiConnector = apiConnector;
    }




    private void afterSavingUserLocation() {

    }

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }


}
