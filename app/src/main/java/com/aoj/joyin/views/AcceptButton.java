package com.aoj.joyin.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by ohad on 2014-05-13.
 */
public class AcceptButton extends Button {

    private String userId;
    public TextView status;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public AcceptButton(Context context) {
        super(context);
    }

    public AcceptButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AcceptButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
