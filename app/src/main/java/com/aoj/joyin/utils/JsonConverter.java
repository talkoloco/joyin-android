package com.aoj.joyin.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.aoj.joyin.chat.Message;
import com.aoj.joyin.model.Group;
import com.aoj.joyin.model.Notification;
import com.aoj.joyin.model.TalkoMessage;
import com.aoj.joyin.model.TalkoModel;
import com.aoj.joyin.model.TalkoUser;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ohad on 2013-11-27.
 */
public class JsonConverter {

    public static List<TalkoModel> convertFromJsonToGroups(JSONArray jsonGroups) throws JSONException {
        List<TalkoModel> groups = new ArrayList<TalkoModel>(jsonGroups.length());
        for (int i = 0; i < jsonGroups.length(); i++) {

            JSONObject groupJson = jsonGroups.getJSONObject(i);
            Group group = (Group) convertFromJsonToGroup(groupJson);
            groups.add(group);
        }
        return groups;
    }

    public static TalkoModel convertFromJsonToGroup(JSONObject jsonObject) throws JSONException {
        Group group = new Group();
        String name = (String) jsonObject.get("groupName");
        group.setName(name);
        Boolean isPrivate = (Boolean) jsonObject.get("isPrivate");
        group.setIsPrivate(isPrivate);
        group.setId((String) jsonObject.get("id"));
        group.setAddress((String) jsonObject.get("address"));
        group.setCreatorId((String) jsonObject.get("creatorId"));
        group.setCreatorName((String) jsonObject.get("creatorName"));
        group.setLastMessage((String) jsonObject.get("lastMessage"));
        group.setLatitude((Double) jsonObject.get("latitude"));
        group.setLongitude((Double) jsonObject.get("longitude"));
        String placeName = jsonObject.get("placeName") == null ? null : jsonObject.get("placeName").toString();
        if (placeName == null || "null".equals(placeName)) {
            placeName = "<No Name>";
        }
        group.setPlaceName(placeName);
        Boolean isAt = (Boolean) jsonObject.get("someoneIsAtPlace");

        Long lastSeenSomeoneAtPlace = (Long) jsonObject.get("lastSeenSomeoneAtPlace");
        Long nowTime = DateTime.now().getMillis();

        Long delta = nowTime - lastSeenSomeoneAtPlace;

        isAt = isAt && (delta < 900000);

        group.setSomeoneIsAtPlace(isAt);
        JSONArray membersArray = (JSONArray) jsonObject.get("members");
        Set<String> members = new HashSet<String>(membersArray.length());
        for (int j = 0; j < membersArray.length(); j++) {
            Object item = membersArray.get(j);
            if (item != null && !item.toString().equals("null"))
                members.add(item.toString());
        }
        group.setMembers(members);

        // TODO set...
        //group.setCreated((DateTime) jsonObject.get("created"));
        //group.setLastSeenSomeoneAtPlace((DateTime) jsonObject.get("lastSeenSomeoneAtPlace"));

        return group;
    }

    public static TalkoUser convertFromFriendJsonToUser(JSONObject jsonUser) throws JSONException {
        TalkoUser user = new TalkoUser();
        user.setId((String) jsonUser.get("id"));
        user.setName((String) jsonUser.get("username"));
        user.setFaceId((String) jsonUser.get("faceId"));
        user.setEmail((String) jsonUser.get("email"));
        String type = (String) jsonUser.get("userType");
        user.setType(type);
        JSONObject facebookObj = (JSONObject) jsonUser.get("facebookDetails");
        user.setPicUrl((String) facebookObj.get("pic"));
        //user.setDeviceToken((String) jsonUser.get("deviceToken"));
        return user;

    }


    public static TalkoUser convertFromFacebookJsonToUser(JSONObject jsonUser) throws JSONException {
        TalkoUser user = new TalkoUser();
        //user.setId((String) jsonUser.get("id"));
        user.setName((String) jsonUser.get("first_name"));
        user.setFaceId((String) jsonUser.get("uid"));
        user.setEmail((String) jsonUser.get("email"));
        user.setType("invitedUser");
        user.setPicUrl((String) jsonUser.get("pic"));
        //user.setDeviceToken((String) jsonUser.get("deviceToken"));
        return user;
    }

    public static TalkoUser convertFromJsonToUser(JSONObject jsonUser) throws JSONException {
        TalkoUser user = new TalkoUser();
        user.setId((String) jsonUser.get("id"));
        user.setName((String) jsonUser.get("username"));
        user.setFaceId((String) jsonUser.get("faceId"));
        user.setEmail((String) jsonUser.get("email"));
        JSONArray pGroups = (JSONArray) jsonUser.get("privateGroups");
        if (pGroups != null) {
            Set<String> privateGroups = new HashSet<String>(pGroups.length());
            for (int i = 0; i < pGroups.length(); i++) {
                privateGroups.add(pGroups.get(i).toString());
            }
            user.setPrivateGroups(privateGroups);
        }

        String type = (String) jsonUser.get("userType");
        user.setType(type);
        JSONObject facebookObj = (JSONObject) jsonUser.get("facebookDetails");
        user.setPicUrl((String) facebookObj.get("pic"));
        user.setActiveGroupId((String) jsonUser.get("activeGroupId"));
        return user;
    }

    public static TalkoMessage convertFromJsonToMessage(JSONObject message) throws JSONException {
        TalkoMessage msg = new Message();
        msg.setSenderId(message.getString("senderId"));
        msg.setSenderIsAtPlace(message.getString("senderIsAtPlace").equals("true") ? true : false);
        msg.setSenderName(message.getString("senderName"));
        msg.setText(message.getString("text"));
        return msg;
    }

    public static List<TalkoModel> convertGetGroupDetailsToUsers(JSONObject jsonMembers) throws JSONException {
        JSONArray arr = jsonMembers.getJSONArray("members");
        List<TalkoModel> members = new ArrayList<TalkoModel>(jsonMembers.length());
        for (int i = 0; i < arr.length(); i++) {
            TalkoModel user = convertFromJsonToUser(arr.getJSONObject(i));
            members.add(user);
        }
        return members;
    }

    public static List<TalkoModel> convertGetGroupDetailsToInvitees(JSONObject jsonInvitees) throws JSONException {
        JSONArray arr = jsonInvitees.getJSONArray("invitees");
        List<TalkoModel> invitees = new ArrayList<TalkoModel>(jsonInvitees.length());
        for (int i = 0; i < arr.length(); i++) {
            TalkoUser user = convertFromJsonToUser(arr.getJSONObject(i));
            user.setType(null);
            invitees.add(user);
        }
        return invitees;
    }

    public static List<TalkoModel> convertGetGroupMessages(JSONArray groupMessages) throws JSONException {

        List<TalkoModel> messages = new ArrayList<TalkoModel>(groupMessages.length());
        for (int i = 0; i < groupMessages.length(); i++) {
            messages.add(convertFromJsonToMessage(groupMessages.getJSONObject(i)));
        }
        return messages;
    }


    public static List<Message> convertFromJsonToMessages(JSONArray messagesJson, Context context) throws JSONException {
        List<Message> messages = new ArrayList<Message>();

        for (int i = 0; i < messagesJson.length(); i++) {
            Message message = new Message();
            JSONObject jsonObject = messagesJson.getJSONObject(i);
            message.setMessage((String) jsonObject.get("text"));
            message.setGroupId((String) jsonObject.get("grId"));
            message.setSenderName((String) jsonObject.get("senderName"));
            message.setSenderIsAtPlace("true".equals(jsonObject.get("senderIsAtPlace")));
            message.setLongitude(jsonObject.getDouble("longitude"));
            message.setLatitude(jsonObject.getDouble("latitude"));
            String senderId = (String) jsonObject.getString("senderId");
            message.setSenderId(senderId);
            Long createdLong = jsonObject.getLong("created");
            DateTime created = new DateTime(createdLong);
            message.setCreated(created);
            SharedPreferences prefs = context.getSharedPreferences("TalkoPreferences", Context.MODE_PRIVATE);
            String userId = prefs.getString("userId", null);
            if (senderId != null && senderId.equals(userId)) {
                message.setMine(true);
            }
            messages.add(message);

        }

        return messages;
    }

    public static void parseJsonToMessages(JSONArray messageArray) {
    }


    public static List<TalkoModel> convertFromJsonToNotifications(JSONArray jsonArray) throws JSONException{
        List<TalkoModel> notifications = new ArrayList<TalkoModel>(jsonArray.length());
        for (int i = 0; i < jsonArray.length(); i++) {
            notifications.add(convertFromJsonToNotification(jsonArray.getJSONObject(i)));
        }
        return notifications;

    }

    public static TalkoModel convertFromJsonToNotification(JSONObject jsonObject) throws JSONException {
        Notification notification = new Notification();
        notification.setId(jsonObject.getString("id"));
        notification.setText(jsonObject.getString("text"));
        notification.setType(jsonObject.getString("nType"));
        notification.setPicUrl(jsonObject.getString("userInContextPicUrl"));
        notification.setGroupId(jsonObject.getString("groupId"));
        notification.setGroupName(jsonObject.getString("groupName"));
        notification.setGroupCreatorId(jsonObject.getString("groupCreatorId"));
        notification.setUserInContextId(jsonObject.getString("userInContextId"));
        notification.setUserInContextName(jsonObject.getString("userInContextName"));
        notification.setRead(jsonObject.getBoolean("wasRead"));
        return notification;
    }
}
