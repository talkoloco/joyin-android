package com.aoj.joyin.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.DateTime;

import java.util.Set;

/**
 * Created by ohad on 2013-11-27.
 */
public class Group implements TalkoModel,Parcelable {
    private String id;
    private String name;
    private String creatorId;
    private String creatorName;
    private Set<String> members;
    private String address;
    private String placeName;
    private Double latitude;
    private Double longitude;
    private DateTime created;
    private Boolean someoneIsAtPlace;
    private DateTime lastSeenSomeoneAtPlace;
    private String lastMessage;
    private boolean isPrivate;

    public Boolean getIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    private Boolean isNew = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Set<String> getMembers() {
        return members;
    }

    public void setMembers(Set<String> members) {
        this.members = members;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public Boolean getSomeoneIsAtPlace() {
        return someoneIsAtPlace;
    }

    public boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public void setSomeoneIsAtPlace(Boolean someoneIsAtPlace) {
        this.someoneIsAtPlace = someoneIsAtPlace;
    }

    public DateTime getLastSeenSomeoneAtPlace() {
        return lastSeenSomeoneAtPlace;
    }

    public void setLastSeenSomeoneAtPlace(DateTime lastSeenSomeoneAtPlace) {
        this.lastSeenSomeoneAtPlace = lastSeenSomeoneAtPlace;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Group(){};

        @Override
    public int describeContents() {
        return 0;
    }

    // Parcelling part
    public Group(Parcel in){
        this.id = in.readString();
        this.name = in.readString();
        this.address = in.readString();
        this.placeName = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.lastMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(this.id);
        parcel.writeString(this.name);
        parcel.writeString(this.address);
        parcel.writeString(this.placeName);
        parcel.writeDouble(this.latitude);
        parcel.writeDouble(this.longitude);
        parcel.writeString(this.lastMessage);

    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Group createFromParcel(Parcel in) {
            return new Group(in);
        }

        public Group[] newArray(int size) {
            return new Group[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;

        Group group = (Group) o;

        if (id != null ? !id.equals(group.id) : group.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
