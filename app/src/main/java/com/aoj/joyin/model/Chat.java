package com.aoj.joyin.model;

/**
 * Created by ohad on 14-11-20.
 */
public class Chat {

    private String id;
    private String  groupId;
    private String groupName;
    private ChatRowUser user;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ChatRowUser getUser() {
        return user;
    }

    public void setUser(ChatRowUser user) {
        this.user = user;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

}
