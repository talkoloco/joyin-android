package com.aoj.joyin.model;

import android.location.Location;

import org.joda.time.DateTime;

import java.util.Set;

/**
 * Created by ohad on 2013-11-29.
 */
public class TalkoUser implements TalkoModel {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;
    private String name;
    private DateTime created;
    private DateTime updated;
    private String deviceToken;
    private String email;
    private String faceId;
    private String picUrl;
    private String type;
    private Location location;
    private Set<String> privateGroups;
    private String activeGroupId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String username) {
        this.name = username;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getUpdated() {
        return updated;
    }

    public void setUpdated(DateTime updated) {
        this.updated = updated;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getFaceId() {
        return faceId;
    }

    public void setFaceId(String faceId) {
        this.faceId = faceId;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Set<String> getPrivateGroups() {
        return privateGroups;
    }

    public void setPrivateGroups(Set<String> privateGroups) {
        this.privateGroups = privateGroups;
    }

    public String getActiveGroupId() {
        return activeGroupId;
    }

    public void setActiveGroupId(String activeGroupId) {
        this.activeGroupId = activeGroupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TalkoUser)) return false;

        TalkoUser talkoUser = (TalkoUser) o;

        if (!faceId.equals(talkoUser.faceId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return faceId.hashCode();
    }
}
