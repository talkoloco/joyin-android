package com.aoj.joyin.model;

/**
 * Created by ohad on 14-11-20.
 */
public class ChatRowUser {

    private String firstName;
    private String picURL;


    public String getPicURL() {
        return picURL;
    }

    public void setPicURL(String picURL) {
        this.picURL = picURL;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

}
