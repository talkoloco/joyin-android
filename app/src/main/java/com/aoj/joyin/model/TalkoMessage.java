package com.aoj.joyin.model;

import org.joda.time.DateTime;

import java.sql.Date;

/**
 * Created by wodom on 21/02/14.
 */
public class TalkoMessage implements TalkoModel {

    private DateTime id;

    public DateTime getId() {
        return id;
    }

    public void setId(DateTime id) {
        this.id = id;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSenderLastSeenAt() {
        return senderLastSeenAt;
    }

    public void setSenderLastSeenAt(String senderLastSeenAt) {
        this.senderLastSeenAt = senderLastSeenAt;
    }

    public Boolean getSenderIsAtPlace() {
        return senderIsAtPlace;
    }

    public void setSenderIsAtPlace(Boolean senderIsAtPlace) {
        this.senderIsAtPlace = senderIsAtPlace;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    private String senderId;
    private String senderName;
    private String text;
    private String senderLastSeenAt;
    private Boolean senderIsAtPlace;
    private Date created;

}
