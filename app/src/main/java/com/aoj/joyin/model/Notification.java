package com.aoj.joyin.model;

/**
 * Created by ohad on 2014-07-30.
 */
public class Notification implements TalkoModel {

    private String id;
    private String text;
    private String picUrl;
    private String groupName;
    private String groupId;
    private String userInContextId;
    private String userInContextName;
    private String type;
    private String groupCreatorId;
    private boolean read;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupCreatorId() {
        return groupCreatorId;
    }

    public void setGroupCreatorId(String groupCreatorId) {
        this.groupCreatorId = groupCreatorId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserInContextId() {
        return userInContextId;
    }

    public void setUserInContextId(String userInContextId) {
        this.userInContextId = userInContextId;
    }

    public String getUserInContextName() {
        return userInContextName;
    }

    public void setUserInContextName(String userInContextName) {
        this.userInContextName = userInContextName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Notification that = (Notification) o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}

