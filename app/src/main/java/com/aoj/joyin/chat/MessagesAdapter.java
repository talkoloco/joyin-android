/*
package com.osc.talkoloco.chat;

import android.app.Fragment;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.osc.talkoloco.R;
import java.util.ArrayList;
import java.util.List;

*/
/**
 * MessagesAdapter is a Custom class to implement custom row in ListView
 * 
 * @author Adil Soomro
 *
 *//*

public class MessagesAdapter extends BaseAdapter {
	private Context mContext;
	private List<Message> mMessages;

	public MessagesAdapter(Context context, List<Message> messages) {
		super();
		this.mContext = context;
		this.mMessages = messages;
	}

    public void setMessages(List<Message> messages){
        this.mMessages = messages;
    }

	@Override
	public int getCount() {
		return mMessages.size();
	}
	@Override
	public Object getItem(int position) {
		return mMessages.get(position);
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Message message = (Message) this.getItem(position);

		ViewHolder holder; 
		if(convertView == null)
		{
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.sms_row, parent, false);
			holder.message = (TextView) convertView.findViewById(R.id.message_text);
            holder.userName = (TextView) convertView.findViewById(R.id.user_name);
            holder.bubbleWrapper = (LinearLayout) convertView.findViewById(R.id.bubble_wrapper);
            holder.isAtPlace = (ImageView) convertView.findViewById(R.id.isAtPlace);
            holder.tick = (ImageView) convertView.findViewById(R.id.tick);
            holder.userNameWrapper = (LinearLayout) convertView.findViewById(R.id.user_name_wrapper);
            holder.messageWrapper = (LinearLayout) convertView.findViewById(R.id.message_wrapper);
            convertView.setTag(holder);
		}
		else
			holder = (ViewHolder) convertView.getTag();
		
		holder.message.setText(message.getMessage());
        holder.userName.setText(message.getSenderName());
        holder.isAtPlace.setVisibility(ImageView.VISIBLE);
        LinearLayout.LayoutParams isAtPlaceParams =new LinearLayout.LayoutParams(20, 20);
        holder.isAtPlace.setLayoutParams(isAtPlaceParams);

        LinearLayout.LayoutParams bubbleParams = (LayoutParams) holder.bubbleWrapper.getLayoutParams();

        if(message.isMine())
        {
            holder.bubbleWrapper.setBackgroundResource(R.drawable.right_bubble);
            //holder.userNameWrapper.removeView(holder.tick);
            //holder.messageWrapper.removeView(holder.isAtPlace);
            //holder.bubbleWrapper.invalidate();
            bubbleParams.weight = 1.0f;
            bubbleParams.gravity=Gravity.RIGHT;
        }
        //If not mine then it is from sender to show orange background and align to left
        else
        {
            holder.bubbleWrapper.setBackgroundResource(R.drawable.left_bubble);
            bubbleParams.weight = 1.0f;
            bubbleParams.gravity=Gravity.LEFT;
        }

        holder.bubbleWrapper.setLayoutParams(bubbleParams);
        holder.message.setTextColor(R.color.textColor);

		return convertView;
	}

	private class ViewHolder
	{
		TextView message;
        LinearLayout bubbleWrapper;
        LinearLayout userNameWrapper;
        LinearLayout messageWrapper;
        TextView userName;
        ImageView isAtPlace;
        ImageView tick;
	}

	@Override
	public long getItemId(int position) {
		//Unimplemented, because we aren't using Sqlite.
		return 0;
	}

}
*/
