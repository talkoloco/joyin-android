/*
package com.osc.talkoloco.chat;

import android.app.ListActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.Session;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.osc.talkoloco.R;
import com.osc.talkoloco.model.Group;
import com.osc.talkoloco.utils.Consts;
import com.osc.talkoloco.utils.JsonConverter;

import org.apache.http.entity.StringEntity;
import org.joda.time.DateTimeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

*/
/**
 * MessageActivity is a main Activity to show a ListView containing Message items
 * 
 * @author Adil Soomro
 *
 *//*

public class MessageActivity extends ListActivity {
	*/
/** Called when the activity is first created. *//*


	MessagesAdapter adapter;
	EditText text;
	static Random rand = new Random();
	static String sender;
    private Group group;
    private String userId;
    private String userName;
    List<Message> messages;
    MessageActivity instance;
    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    String SENDER_ID = "145042884974";
    ProgressBar progressBar;



    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        instance = this;
		setContentView(R.layout.main);

        View currentView = this.findViewById(android.R.id.content);
        progressBar = (ProgressBar) currentView.findViewById(R.id.progress_bar);
        progressBar.setVisibility(ProgressBar.VISIBLE);

        Bundle data = getIntent().getExtras();
        group = (Group) data.getParcelable("group");

        SharedPreferences preferences = instance.getSharedPreferences("TalkoPreferences", Context.MODE_PRIVATE);
        userId = preferences.getString("userId", null);
        userName = preferences.getString("userName", null);

        text = (EditText) this.findViewById(R.id.text);
        // TODO
		//sender = Utility.sender[rand.nextInt( Utility.sender.length-1)];
        this.setTitle(group.getName());
        messages = new ArrayList<Message>();
        adapter = new MessagesAdapter(instance, messages);
        setListAdapter(adapter);

        loadGroupMessages();
*/
/*
		messages.add(new Message("Hello", false));
		messages.add(new Message("Hi!", true));
		messages.add(new Message("Wassup??", false));
		messages.add(new Message("nothing much, working on speech bubbles.", true));
		messages.add(new Message("you say!", true));
		messages.add(new Message("oh thats great. how are you showing them", false));*//*

		//addNewMessage(new Message("mmm, well, using 9 patches png to show them.", true));
	}
	public void sendMessage(View v)
	{
		String newMessage = text.getText().toString().trim();
		if(newMessage.length() > 0)
		{
			text.setText("");
			addNewMessage(new Message(newMessage, true));
            sendMessageToServer(newMessage);
			//new SendMessage().execute(newMessage);
		}
	}

    private class SendMessage extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            String msg = params[0];
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(instance);
                }
                Bundle data = new Bundle();
                data.putString("my_message", "Hello World");
                data.putString("my_action",
                        "com.google.android.gcm.demo.app.ECHO_NOW");
                String id = Integer.toString(msgId.incrementAndGet());
                gcm.send(SENDER_ID + "@gcm.googleapis.com", id, data);
                msg = "Sent message";
            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
            }
            return msg;
        }
    }


    private void sendToGCM(String newMessage){
        new SendMessage().execute(newMessage);
    }


    private void sendMessageToServer(String messageToSend){
        Session session = Session.getActiveSession();
        String accessToken = session.getAccessToken();

        AsyncHttpClient client = new AsyncHttpClient();

        SharedPreferences preferences = instance.getSharedPreferences("TalkoPreferences", Context.MODE_PRIVATE);
        String userCookie = preferences.getString("userCookie", null);
        client.addHeader("username", userCookie);

        JSONObject jsonParams = new JSONObject();

        StringEntity entity;

        try {
            String userId = preferences.getString("userId", null);
            jsonParams.put("groupId", group.getId());
            jsonParams.put("userId", userId);
            jsonParams.put("userName", userName);
            jsonParams.put("text", messageToSend);
            Long time = DateTimeUtils.currentTimeMillis();
            jsonParams.put("date", Long.toString(time));
            jsonParams.put("latitude", "0");
            jsonParams.put("longitude", "0");
            jsonParams.put("chunk", "15");
            jsonParams.put("phoneType","android");
            entity = new StringEntity(jsonParams.toString());

        }catch (Exception e){
            throw new RuntimeException(e);
        }

        client.post(instance, Consts.URL + "newGroupMessage", entity, "application/json", new JsonHttpResponseHandler() {

            public void onSuccess(int statusCode, org.apache.http.Header[] headers, java.lang.String responseBody) {
                System.out.print("success");
            }

            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, java.lang.String responseBody, java.lang.Throwable e) {
                throw new RuntimeException(e);
            }
        });
    }


	void addNewMessage(Message m)
	{
		messages.add(m);
		adapter.notifyDataSetChanged();
		getListView().setSelection(messages.size()-1);
	}

    private void loadGroupMessages(){
        Session session = Session.getActiveSession();
        String accessToken = session.getAccessToken();

        AsyncHttpClient client = new AsyncHttpClient();

        SharedPreferences preferences = this.getSharedPreferences("TalkoPreferences", Context.MODE_PRIVATE);
        String userCookie = preferences.getString("userCookie", null);
        client.addHeader("username", userCookie);

        JSONObject jsonParams = new JSONObject();
        try {
            String userId = preferences.getString("userId", null);
            jsonParams.put("groupId", group.getId());
            jsonParams.put("chunk", "15");

            StringEntity entity = new StringEntity(jsonParams.toString());
            client.post(this, Consts.URL + "groupMessages", entity, "application/json", new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, org.apache.http.Header[] headers, java.
                        lang.String responseBody) {
                    System.out.print("Talko User Details: " + responseBody);
                    JSONArray jsonArray = null;
                    try {
                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                        jsonArray = new JSONArray(responseBody);
                        messages = JsonConverter.convertFromJsonToMessages(jsonArray, instance);
                        adapter.setMessages(messages);
                        adapter.notifyDataSetChanged();
                        getListView().setSelection(messages.size()-1);

                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }

                @Override
                public void onFailure(int statusCode, org.apache.http.Header[] headers, java.lang.String responseBody, java.lang.Throwable e) {
                    throw new RuntimeException(e);
                }

            });

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}*/
