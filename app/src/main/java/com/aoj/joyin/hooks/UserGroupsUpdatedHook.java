package com.aoj.joyin.hooks;

import com.aoj.joyin.model.TalkoModel;

import java.util.List;
import java.util.Observable;

/**
 * Created by I071873 on 5/17/14.
 */
public class UserGroupsUpdatedHook extends Observable {

    public List<TalkoModel> getGroupsList() {
        return groupsList;
    }

    public void setGroupsList(List<TalkoModel> groupsList) {
        this.setChanged();
        this.groupsList = groupsList;
    }

    private List<TalkoModel> groupsList;

}
