package com.aoj.joyin.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.aoj.joyin.R;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.aoj.joyin.activites.AppTabContainerActivity;
import com.aoj.joyin.activites.LoginActivity;

import java.util.Arrays;

/**
 * Created by I071873 on 1/3/14.
 */
public class LoginFragment extends Fragment {
    private static final String TAG = "LoginFragment";
    private UiLifecycleHelper uiHelper;
    protected View viewRef;
    private LoginFragment that = this;
    LoginButton authButton;
    ProgressBar progressBar;
    LinearLayout authButtonWrapper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_activity, container, false);
        authButton = (LoginButton) view.findViewById(R.id.authButton);
        authButton.setFragment(this);
        authButton.setReadPermissions(Arrays.asList("basic_info","user_likes", "user_status"));
        authButton.setVisibility(Button.VISIBLE);
        authButtonWrapper = (LinearLayout)view.findViewById(R.id.authButtonWrapper);
        progressBar = (ProgressBar)view.findViewById(R.id.loginProgressBar);
        progressBar.setVisibility(ProgressBar.GONE);
        view.setVisibility(View.INVISIBLE);
        this.viewRef = view;
        return view;

    }

    public void navigateToIFollow(){
        Intent intent = new Intent(getActivity(), AppTabContainerActivity.class);
        startActivity(intent);
    }


    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {
            Log.i(TAG, "Logged in...");
        } else if (state.isClosed()) {
            Log.i(TAG, "Logged out...");
        }

        switch (state) {
            case OPENED:
                authButton.setVisibility(Button.GONE);
                progressBar.setVisibility(ProgressBar.VISIBLE);
                break;
            case OPENING:
                authButton.setVisibility(Button.GONE);
                progressBar.setVisibility(ProgressBar.VISIBLE);
                break;
            case CLOSED_LOGIN_FAILED:
                authButton.setVisibility(Button.VISIBLE);
                progressBar.setVisibility(ProgressBar.GONE);
                Toast.makeText(that.getActivity(),
                        "Login failed, check your facebook credentials",
                        Toast.LENGTH_LONG).show();
                break;

        }
    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
        Session session = Session.getActiveSession();
        if (session.isOpened()) {
            authButton.setVisibility(Button.GONE);
            progressBar.setVisibility(ProgressBar.VISIBLE);
            LoginActivity active= (LoginActivity)getActivity();
            active.doLogin(session);
        } else{
            this.viewRef.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

}
