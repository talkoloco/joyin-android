package com.aoj.joyin.fragments.adapters;

import android.app.ActionBar;
import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.aoj.joyin.R;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.activites.AppTabContainerActivity;
import com.aoj.joyin.fragments.ChatFragment;
import com.aoj.joyin.fragments.GroupManagerFragment;
import com.aoj.joyin.fragments.NearbyFragment;
import com.aoj.joyin.model.Group;
import com.aoj.joyin.model.TalkoModel;
import com.aoj.joyin.views.AutoResizeTextView;

import java.util.List;

/**
 * Created by ohad on 2013-11-24.
 */

public class MyPlacesItemAdapter extends BaseAdapter {
    private Context mContext;
    private AppTabContainerActivity placeActivity;
    private LayoutInflater mInflater;
    private Paint mPaint;
    public void setGroups(List<TalkoModel> groups) {
        this.groups = groups;
    }

    private List<TalkoModel> groups;

    public MyPlacesItemAdapter(Activity c) {
        mContext = c;
        placeActivity = (AppTabContainerActivity)c;
        mInflater = LayoutInflater.from(c);
    }

    public int getCount() {
        if (groups!=null)
            return groups.size();
        else
            return 0;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {



        Group group = (Group)groups.get(position);
        if (convertView==null){
            RelativeLayout layout = (RelativeLayout)mInflater.inflate(R.layout.grid_view_item, null);
            if(group.getIsNew()){
                layout.setBackgroundColor(Color.BLUE);
            }
            AutoResizeTextView header = (AutoResizeTextView)layout.getChildAt(3);
            AutoResizeTextView groupInfo = (AutoResizeTextView)layout.getChildAt(4);
            ImageView img =  (ImageView)layout.findViewById(R.id.status);
            if (group.getSomeoneIsAtPlace()){
                img.setBackgroundResource(R.drawable.spin_animation);
                // Get the background, which has been compiled to an AnimationDrawable object.
                AnimationDrawable frameAnimation = (AnimationDrawable) img.getBackground();
                // Start the animation (looped playback by default).
                frameAnimation.start();
            } else {
                img.setBackgroundResource(R.drawable.offline_red);
            }


            groupInfo.setText(group.getMembers().size() +" Members");
            header.setText(group.getName());
            
            header.setTag(group);
            header.setMaxLines(1);
            header.setTextSize(60);
            header.resizeText();

            Group selectedGroup = (Group) groups.get(position);

            Button groupDetailsButton = (Button)layout.getChildAt(1);

            groupDetailsButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    ActionBar actionBar = placeActivity.getActionBar();

                    placeActivity.showFragmentByType();
                    Group groupie = (Group) groups.get(position);
                    placeActivity.prevFragment = 1;
                    placeActivity.setSelectedGroup(groupie);
                    placeActivity.getViewPager().setCurrentItem(1,false);
                    placeActivity.getGroupDetails(groupie.getId());

                    FragmentManager fragmentManger = placeActivity.getSupportFragmentManager();
                    String fragmentTag = placeActivity.getmAdapter().makeFragmentName(placeActivity.getViewPager().getId(), 1);
                    GroupManagerFragment groupMangerFragment = (GroupManagerFragment) fragmentManger.findFragmentByTag(fragmentTag);

                    if (groupie.getIsPrivate()) {
                        groupMangerFragment.hideButtons();
                    }
                    groupMangerFragment.reloadUsersAndTitle();
                    groupMangerFragment.updateUserList();

                    String nearbyFragmentTag = placeActivity.getmAdapter().makeFragmentName(placeActivity.getViewPager().getId(), 2);
                    NearbyFragment nearbyFragment = (NearbyFragment) fragmentManger.findFragmentByTag(nearbyFragmentTag);
                    nearbyFragment.updateNearbyUsers();
                }
            });

            Button groupButton = (Button)layout.getChildAt(0);
            groupButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Group groupie = (Group)groups.get(position);
                    placeActivity.setSelectedGroup(groupie);

                    ChatFragment chatFragment = getChatFragment(placeActivity);
                    if (chatFragment!=null){
                        chatFragment.setCachedGroupName(null);
                    }

                    placeActivity.getViewPager().setCurrentItem(3,false);

                    chatFragment = getChatFragment(placeActivity);
                  //  chatFragment.setActiveGroup(groupy);
                    chatFragment.getUserGroups();

                    // Get tracker.
                    Tracker t = ((TalkoApp) placeActivity.getApplication()).getTracker(
                            TalkoApp.TrackerName.APP_TRACKER);
                    // Build and send an Event.
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("chat")
                            .setAction("load.chat")
                            .setLabel("chat.label")
                            .build());
                }
            });


            convertView = layout;

        }
        convertView.setTag(group);
        return convertView;
    }

    private ChatFragment getChatFragment(AppTabContainerActivity activity){
        FragmentManager fragmentManger =  activity.getSupportFragmentManager();
        String fragmentTag = activity.getmAdapter().makeFragmentName(activity.getViewPager().getId(), 3);
        ChatFragment chatFragment = (ChatFragment)fragmentManger.findFragmentByTag(fragmentTag);
        return chatFragment;
    }
}