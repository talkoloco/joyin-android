package com.aoj.joyin.fragments;


import android.app.ActionBar;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.andtinder.model.CardModel;
import com.andtinder.model.Orientations;
import com.andtinder.view.CardContainer;
import com.andtinder.view.SimpleCardStackAdapter;
import com.aoj.joyin.R;
import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.activites.AppTabContainerActivity;
import com.aoj.joyin.apis.CallableAbstract;
import com.aoj.joyin.apis.GroupsApi;
import com.aoj.joyin.fragments.adapters.MyPlacesItemAdapter;
import com.aoj.joyin.hooks.UserGroupsUpdatedHook;
import com.aoj.joyin.model.Group;
import com.aoj.joyin.model.TalkoModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

/**
 * Created by I071873 on 1/11/14.
 */
public class MyPlacesFragment extends Fragment implements Observer {
    private PullToRefreshLayout mPullToRefreshLayout;
    private TalkoApp app ;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        app = (TalkoApp) this.getActivity().getApplication();

        app.getObservable(UserGroupsUpdatedHook.class).addObserver(this);

        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.nearby_fragment_dialog, container, false);
        CardContainer mCardContainer = (CardContainer) rootView.findViewById(R.id.layoutview);
        Resources r = getResources();
        SimpleCardStackAdapter adapter = new SimpleCardStackAdapter(getActivity());
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.id.userPic);
        mCardContainer.setOrientation(Orientations.Orientation.Ordered);
        adapter.add(new CardModel("Title1", "Description goes here", "http://hdwallpaperfresh.com/wp-content/uploads/2013/12/3D-Sexy-Girl-Tattoos-Wallpaper.jpg", "http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg", "http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg", "http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg"));
        adapter.add(new CardModel("Title1", "Description goes here",  "http://hdwallpaperfresh.com/wp-content/uploads/2013/12/3D-Sexy-Girl-Tattoos-Wallpaper.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg"));
        adapter.add(new CardModel("Title1", "Description goes here",  "http://hdwallpaperfresh.com/wp-content/uploads/2013/12/3D-Sexy-Girl-Tattoos-Wallpaper.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg"));

        mCardContainer.setAdapter(adapter);
        mCardContainer.setGravity(Gravity.TOP);


        return rootView;

    }

    public void getUserGroups() {
        GroupsApi groupsApi = new GroupsApi(app);
        groupsApi.getGroups().execute(new CallableAbstract() {
            public void callback(List<TalkoModel> groupList, String response) {
               ((UserGroupsUpdatedHook)app.getObservable(UserGroupsUpdatedHook.class)).setGroupsList(groupList);
                app.getObservable(UserGroupsUpdatedHook.class).notifyObservers();
                mPullToRefreshLayout.setRefreshComplete();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.talko, menu);
      //  inflater.inflate(R.menu.main, menu);

        super.onCreateOptionsMenu(menu,inflater);
    }

    public void getOrDisplayUserGroups(){
           if(app.getLocalCache().containsKey("groups")){
               this.updateGridView((List<TalkoModel>)app.getLocalCache().get("groups"));
               return;
           }
         getUserGroups();
    }

    public void updateNoPlaceViews(View rView, List<TalkoModel> groupList, TextView noPlacesTextView, ImageView noPlacesImageView){
        if (noPlacesTextView==null)
            noPlacesTextView = (TextView)rView.findViewById(R.id.noPlaces);
        if (noPlacesImageView==null)
            noPlacesImageView = (ImageView)rView.findViewById(R.id.arrow);

        if (groupList!=null && groupList.size() > 0){
            noPlacesTextView.setVisibility(TextView.INVISIBLE);
            noPlacesImageView.setVisibility(ImageView.INVISIBLE);
        } else {
            noPlacesTextView.setVisibility(TextView.VISIBLE);
            noPlacesImageView.setVisibility(ImageView.VISIBLE);
        }
    }

    public  List<TalkoModel> sortGroupsByOnline(List<TalkoModel> groupList) {
        List<Group> gList =  (List<Group>) (List<?>) groupList;
        Collections.sort(gList,new Comparator<Group>(){
            public int compare(Group s1,Group s2){
                Boolean sap1 = s1.getSomeoneIsAtPlace();
                Boolean sap2 = s2.getSomeoneIsAtPlace();
                if (sap1!=sap2){
                    return -1*sap1.compareTo(sap2);
                } else {
                    return s1.getName().compareTo(s2.getName());
                }
            }});

        return (List<TalkoModel>)(List<?>)gList;
    }

    public void updateGridView(List<TalkoModel> groups) {
        GridView gridview = (GridView) rootView.findViewById(R.id.gridview);
        TextView textView = (TextView) rootView.findViewById(R.id.noPlaces);
        ImageView imageView = (ImageView) rootView.findViewById(R.id.arrow);
        updateNoPlaceViews(null, groups, textView, imageView);
        MyPlacesItemAdapter adapter = new MyPlacesItemAdapter(this.getActivity());
        adapter.setGroups(groups);
        gridview.setAdapter(adapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        AppTabContainerActivity activity = (AppTabContainerActivity)this.getActivity();

        switch (item.getItemId()) {
            case R.id.action_cart:
                activity.displayGroupsNearBy();
                return true;
            case R.id.profile:
                activity.logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void selectGroup() {

        ((AppTabContainerActivity) this.getActivity()).getViewPager().setCurrentItem(1,false);

    }


    @Override
    public void update(Observable observable, Object o) {
      //  ((AppTabContainerActivity) this.getActivity()).appMenu.removeItem(0);
        if(observable instanceof  UserGroupsUpdatedHook){
            UserGroupsUpdatedHook userGroupsUpdatedHook = (UserGroupsUpdatedHook) observable;
            app.getLocalCache().put("groups",userGroupsUpdatedHook.getGroupsList());
            updateGridView((sortGroupsByOnline(userGroupsUpdatedHook.getGroupsList())));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onHiddenChanged  (boolean hidden){
        super.onHiddenChanged( hidden);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        AppTabContainerActivity activity = (AppTabContainerActivity)this.getActivity();
        if (activity==null) return;
        ActionBar actionBar = activity.getActionBar();
        if (isVisibleToUser){
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        } else {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        }
    }

}


