package com.aoj.joyin.fragments;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aoj.joyin.R;
import com.facebook.FacebookException;
import com.facebook.Session;
import com.facebook.widget.WebDialog;
import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.activites.AppTabContainerActivity;
import com.aoj.joyin.apis.CallableAbstract;
import com.aoj.joyin.apis.GroupsApi;
import com.aoj.joyin.apis.UserApi;
import com.aoj.joyin.fragments.adapters.ManageGroupListAdapter;
import com.aoj.joyin.hooks.UserGroupsUpdatedHook;
import com.aoj.joyin.model.Group;
import com.aoj.joyin.model.TalkoFacebookUsers;
import com.aoj.joyin.model.TalkoModel;
import com.aoj.joyin.model.TalkoUser;
import com.aoj.joyin.model.TalkoUsers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by I071873 on 1/25/14.
 */
public class GroupManagerFragment  extends Fragment{

    private LayoutInflater cachedInflater;
    public static String TAG= "GROUP.MANAGER";
    public Set<String> getFacebookIds() {
        return faceIds;
    }
    public void setFacebookIds(Set<String> faceIds) {
        this.faceIds = faceIds;
    }
    private Set<String> faceIds;
    private ManageGroupListAdapter adapter;
    TalkoApp app = null;
    Button addNearbyBtn;
    Button addFriendsBtn;
    TextView groupNameTextView;


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
    }

    public void hideButtons(){
        AppTabContainerActivity activity = (AppTabContainerActivity)this.getActivity();
        Group currentGroup = activity.getSelectedGroup();

        if (currentGroup!=null) {
            if (addFriendsBtn!=null && currentGroup.getIsPrivate()) {
                addFriendsBtn.setVisibility(Button.GONE);
            }

            if (addNearbyBtn!=null && currentGroup != null && currentGroup.getIsPrivate()) {
                addNearbyBtn.setVisibility(Button.GONE);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        AppTabContainerActivity activity = (AppTabContainerActivity)this.getActivity();

        switch (item.getItemId()) {
            case R.id.profile:
                activity.logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        AppTabContainerActivity activity = (AppTabContainerActivity)this.getActivity();
        Group currentGroup = activity.getSelectedGroup();

        cachedInflater = inflater;
        View rootView = inflater.inflate(R.layout.group_details, container, false);



        addFriendsBtn = (Button)rootView.findViewById(R.id.addFriendsBtn);

        if (currentGroup!=null && currentGroup.getIsPrivate()){
            addFriendsBtn.setVisibility(Button.GONE);
        }

        addFriendsBtn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                sendRequestDialog();
            }
        });

        addNearbyBtn = (Button)rootView.findViewById(R.id.addNearByBtn);

        if (currentGroup!=null && currentGroup.getIsPrivate()){
            addNearbyBtn.setVisibility(Button.GONE);
        }

        addNearbyBtn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                showNearbyDialog();
            }
        });

        groupNameTextView = (TextView)rootView.findViewById(R.id.groupName);

        groupNameTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String groupName = v.getText().toString();
                boolean isDone = onDonePressed(groupName, v, faceIds);
                if (isDone){
                    faceIds = null;
                }
                return isDone;
            }
        });

        app = ((TalkoApp) this.getActivity().getApplication());

        String userId = app.getUser().getId();


        if (currentGroup!=null && currentGroup.getCreatorId()!=null && !currentGroup.getCreatorId().equals(userId)){
            groupNameTextView.setKeyListener(null);
        }


        final GroupManagerFragment that = this;

        adapter = new ManageGroupListAdapter(getActivity(), null);

        final ListView list = (ListView)rootView.findViewById(R.id.listView);
        list.setClickable(true);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                AppTabContainerActivity activity = (AppTabContainerActivity)that.getActivity();

                Group group = activity.getSelectedGroup();
                if (group.getIsPrivate()){
                    return;
                }

                TalkoUser selectedUser = (TalkoUser)list.getItemAtPosition(position);
                TalkoUser currentUser = app.getUser();

                if (selectedUser.equals(currentUser))
                    return;

                if (selectedUser.getType().equals("invitedUser"))
                    return;

                Set<String> curGroups = currentUser.getPrivateGroups();
                Set<String> selGroups = selectedUser.getPrivateGroups();

                if (selGroups!=null) {
                    selGroups.retainAll(curGroups);
                }

                if (selGroups==null || selGroups.size()==0) {
                    activity.setSelectedGroup(null);
                    Set<String> fIds = new HashSet<String>();
                    fIds.add(selectedUser.getFaceId());
                    String privateGroupName = selectedUser.getName() + " & " + currentUser.getName();
                    createGroup(privateGroupName, fIds, selectedUser.getId());
                    FragmentManager fragmentManger = activity.getSupportFragmentManager();
                    String fragmentTag = activity.getmAdapter().makeFragmentName(activity.getViewPager().getId(), 3);
                    ChatFragment chatFragment = (ChatFragment) fragmentManger.findFragmentByTag(fragmentTag);
                    chatFragment.setCachedGroupName(privateGroupName);
                    activity.getViewPager().setCurrentItem(3,false);
                } else {
                    String groupId = (String)selGroups.iterator().next();
                    GroupsApi groupsApi = new GroupsApi(((TalkoApp) that.getActivity().getApplication()), Group.class);
                    groupsApi.getGroup(groupId).execute(new CallableAbstract() {
                        @Override
                        public void callback(TalkoModel model, String response) {
                            AppTabContainerActivity activity = (AppTabContainerActivity)that.getActivity();
                            activity.getApplication();
                            Group group = (Group)model;
                            activity.setSelectedGroup(group);
                            FragmentManager fragmentManger = activity.getSupportFragmentManager();
                            String fragmentTag = activity.getmAdapter().makeFragmentName(activity.getViewPager().getId(), 3);
                            ChatFragment chatFragment = (ChatFragment) fragmentManger.findFragmentByTag(fragmentTag);
                            //chatFragment.setActiveGroup(group);
                            WebView wv = chatFragment.wv;
                            wv.loadUrl("about:blank");
                            wv = null;
                            chatFragment.setCachedGroupName(group.getName());
                            activity.getViewPager().setCurrentItem(3,false);
                            chatFragment.getUserGroups(group);
                        }
                    });
                    }
                }

        });


        list.setAdapter(adapter);
        this.setRetainInstance(true);

        // BEGIN_INCLUDE (inflate_set_custom_view)
        // Inflate a "Done" custom action bar view to serve as the "Up" affordance.



        return rootView;
    }

    private boolean onDonePressed(String groupName, View v, Set<String> facebookIds){
        if ("".equals(groupName)) {
            Toast.makeText(this.getActivity().getApplicationContext(),
                    "Meetup was not created.. name can't be empty!",
                    Toast.LENGTH_LONG).show();
            return false;
        }

        AppTabContainerActivity active = (AppTabContainerActivity)this.getActivity();
        Group group = active.getSelectedGroup();

        if (group==null){ // create group mode
            createGroup(groupName, facebookIds, null);
            Toast.makeText(this.getActivity(), "Group was created", Toast.LENGTH_LONG).show();
            return true;

        } else if (!group.getName().equals(groupName)) {
            updateGroupName(group.getId(), groupName);
            InputMethodManager imm = (InputMethodManager)this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            return true;
        }

        return false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        final GroupManagerFragment that = this;

        final AppTabContainerActivity activity = (AppTabContainerActivity)this.getActivity();

        if (activity==null)
            return;

        final ActionBar actionBar = activity.getActionBar();

        if (isVisibleToUser && cachedInflater!=null) {

            final View customActionBarView = cachedInflater.inflate(
                    R.layout.actionbar_custom_view_done, null);
            customActionBarView.findViewById(R.id.actionbar_done).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            TextView mTextView = (TextView)getView().findViewById(R.id.groupName);
                            String groupName = mTextView.getText().toString();
                            onDonePressed(groupName, v, that.getFacebookIds());
                            faceIds = null;

                            if (activity.getSelectedGroup()==null){
                                activity.getViewPager().setCurrentItem(0,false);
                            } else if (activity.prevFragment==3){
                                FragmentManager fragmentManger = activity.getSupportFragmentManager();
                                String fragmentTag = activity.getmAdapter().makeFragmentName(activity.getViewPager().getId(), 3);
                                ChatFragment chatFragment = (ChatFragment) fragmentManger.findFragmentByTag(fragmentTag);
                                //chatFragment.setActiveGroup(group);
                                WebView wv = chatFragment.wv;
                                wv.loadUrl("about:blank");
                                wv = null;
                                activity.getViewPager().setCurrentItem(3,false);
                                //activity.setSelectedGroup(group);
                                chatFragment.getUserGroups();
                                chatFragment.setCachedGroupName(null);

                            } else {
                                activity.getViewPager().setCurrentItem(0,false);
                                /*FragmentManager fragmentManger =  activity.getFragmentManager();
                                String fragmentTag = activity.getmAdapter().makeFragmentName(activity.getViewPager().getId(), 0);
                                MyPlacesFragment myPlacesFragment = (MyPlacesFragment)fragmentManger.findFragmentByTag(fragmentTag);*/
                            }
                        }
                    });
            // Show the custom action bar view and hide the normal Home icon and title.
            actionBar.setDisplayOptions(
                    ActionBar.DISPLAY_SHOW_CUSTOM,
                    ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME
                            | ActionBar.DISPLAY_SHOW_TITLE);
            actionBar.setCustomView(customActionBarView);
        }
        else {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
        }
    }

    private void updateGroupName(String groupId, String groupName) {
        TalkoApp app = ((TalkoApp) this.getActivity().getApplication());
        GroupsApi groupsApi = new GroupsApi(app, Group.class);
        groupsApi.updateGroupName(groupId, groupName).execute(new CallableAbstract() {
            public void callback(TalkoModel group, String response) {
                Log.d("Talko", response);
                //afterUpdateGroupName(group, response);
            }
        });
    }

    private void createGroup(String groupName, Set<String> facebookIds, final String partnerId) {

        final Set<String> _facebookIds = facebookIds;
        GroupsApi groupsApi = new GroupsApi(((TalkoApp) this.getActivity().getApplication()), Group.class);

        if (partnerId==null) {
            groupsApi.createGroup(groupName).execute(new CallableAbstract() {
                @Override
                public void callback(TalkoModel model, String response) {
                    afterCreateGroups(model, response, _facebookIds);
                    GroupsApi groupsApi = new GroupsApi(app);
                    final Group createdGroup = (Group) model;
                    groupsApi.getGroups().execute(new CallableAbstract() {
                        public void callback(List<TalkoModel> groupList, String response) {
                            List<Group> groups = (List<Group>) (List<?>) groupList;
                            groups.get(groups.indexOf(createdGroup)).setIsNew(true);
                            ((UserGroupsUpdatedHook) app.getObservable(UserGroupsUpdatedHook.class)).setGroupsList(groupList);
                            app.getObservable(UserGroupsUpdatedHook.class).notifyObservers();
                        }
                    });

                }
            });
        } else {
            groupsApi.createPrivateGroup(groupName, partnerId).execute(new CallableAbstract() {
                @Override
                public void callback(TalkoModel model, String response) {
                    afterCreatePrivateGroup(model, response, _facebookIds);
                }
            });
        }

    }

    private void afterCreatePrivateGroup(TalkoModel model, String response, Set<String> facebookIds) {
        Group group = (Group)model;
        AppTabContainerActivity activity = ((AppTabContainerActivity)getActivity());
        activity.setSelectedGroup(group);

        UserApi userApi = new UserApi(app, TalkoUsers.class);
        userApi.inviteFacebookUsers(group, facebookIds).execute(new CallableAbstract() {
            public void callback(List<TalkoModel> users, String response) {
                // currently does nothing
            }
        });

        FragmentManager fragmentManger =  activity.getSupportFragmentManager();
        String fragmentTag = activity.getmAdapter().makeFragmentName(activity.getViewPager().getId(), 3);
        ChatFragment chatFragment = (ChatFragment)fragmentManger.findFragmentByTag(fragmentTag);
        //chatFragment.setActiveGroup(group);
        chatFragment.getUserGroups();
    }

    private void afterCreateGroups(TalkoModel model, String response, Set<String> facebookIds) {
        Log.d("Talko", response);
        Group group = (Group)model;
        TalkoApp app = (TalkoApp) getActivity().getApplication();
        UserApi userApi = new UserApi(app, TalkoUsers.class);
        final AppTabContainerActivity tabActivity = ((AppTabContainerActivity)getActivity());
        FragmentManager fragmentManger =  tabActivity.getSupportFragmentManager();
        String fragmentTag = tabActivity.getmAdapter().makeFragmentName(tabActivity.getViewPager().getId(), 0);
        MyPlacesFragment myPlacesFragment = (MyPlacesFragment)fragmentManger.findFragmentByTag(fragmentTag);
        myPlacesFragment.getUserGroups();

        if (facebookIds!=null && facebookIds.size()>0) {

            userApi.inviteFacebookUsers(group, facebookIds).execute(new CallableAbstract() {
                public void callback(List<TalkoModel> users, String response) {
                    tabActivity.getViewPager().setCurrentItem(0,false);

                }
            });

        } else {
            tabActivity.getViewPager().setCurrentItem(0, false);
        }

    }

    void showNearbyDialog() {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        NearbyFragment newFragment = NearbyFragment.newInstance(1);
        newFragment.show(ft, "dialog");
    }

    public void onAttach (Activity activity){

        super.onAttach(activity);
        Log.i(TAG, "onAttach...");
    }

    @Override
    public void onStart(){
        super.onStart();
        Log.i(TAG, "onStart...");

    }

    @Override
    public void onResume(){
        super.onResume();
        Log.i(TAG, "onResume...");

    }

    public void onInflate (Activity activity, AttributeSet attrs, Bundle savedInstanceState){
        super.onInflate(activity,attrs,savedInstanceState);
        Log.i(TAG, "Logged in...");
        // Toast.makeText(this.getActivity(), "hello world", Toast.LENGTH_LONG).show();
    }


    public void onHiddenChanged  (boolean hidden){
        faceIds = null;
        super.onHiddenChanged( hidden);
    }

    public void updateUserList(){
        AppTabContainerActivity active = (AppTabContainerActivity)this.getActivity();
        Group group = active.getSelectedGroup();
        GroupsApi groupsApi = new GroupsApi(((TalkoApp) getActivity().getApplication()), TalkoUser.class);
        groupsApi.getGroupMembers(group.getId()).execute(new CallableAbstract() {
            public void callback(Map<String, List<TalkoModel>> map, String response){
                List<TalkoModel> members = map.get("members");
                List<TalkoModel> invitees = map.get("invitees");
                members.addAll(invitees);
                afterUpdateUserList(members, response);
            }
        });
    }

    private void afterUpdateUserList(List<TalkoModel> groupUsers, String response) {
        adapter.setUsers(groupUsers);
        adapter.notifyDataSetChanged();
    }

    public void reloadUsers(List<TalkoModel> invitedUsers){

        List<TalkoModel> currentMembers = adapter.getUsers();
        if (currentMembers!=null) {
            invitedUsers.removeAll(currentMembers);
            currentMembers.addAll(invitedUsers);
        } else {
            currentMembers = invitedUsers;
        }
        adapter.setUsers(currentMembers);
        adapter.notifyDataSetChanged();

    }

    public void reloadUsersAndTitle(){
        AppTabContainerActivity active = (AppTabContainerActivity)this.getActivity();
        Group group = active.getSelectedGroup();
        ((EditText)this.getView().findViewById(R.id.groupName)).setHint("Group Name");
        if (group!=null) {
            ((EditText)this.getView().findViewById(R.id.groupName)).setText(group.getName());
        } else {
            ((EditText)this.getView().findViewById(R.id.groupName)).setText("");
            adapter.setUsers(null);
            adapter.notifyDataSetChanged();
        }
    }

    private void sendRequestDialog() {
        Bundle params = new Bundle();
        params.putString("message", "Talkoco!");
        params.putString("title", "Invite");

        WebDialog requestsDialog = (
                new WebDialog.RequestsDialogBuilder(getActivity(),
                        Session.getActiveSession(),
                        params))
                .setOnCompleteListener(new WebDialog.OnCompleteListener() {

                    @Override
                    public void onComplete(Bundle values,
                                           FacebookException error) {
                        if (error != null) {
                            Toast.makeText(getActivity().getApplicationContext(),
                                    "Invite cancelled",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            final String requestId = values.getString("request");

                            if (faceIds==null)
                                faceIds = new HashSet<String>(values.keySet().size());

                            for (String key : values.keySet()){
                                if (key.contains("to")){
                                    String value = (String)values.get(key);
                                    faceIds.add(value);
                                }
                            }
                            inviteFacebookFriends();
                        }
                    }

                })
                .build();
        requestsDialog.show();
    }

    private void inviteFacebookFriends() {
        if (faceIds==null || faceIds.size()==0)
            return;
        TalkoApp app = ((TalkoApp) this.getActivity().getApplication());
        AppTabContainerActivity active = (AppTabContainerActivity)this.getActivity();
        Group group = active.getSelectedGroup();

        if (group!=null) {
            UserApi userApi = new UserApi(app, TalkoUsers.class);
            userApi.inviteFacebookUsers(group, faceIds).execute(new CallableAbstract() {
                public void callback(List<TalkoModel> users, String response) {
                    afterInviteUsers(users, response);
                    faceIds = null;
                }
            });
        } else {
            UserApi userApi = new UserApi(app, TalkoFacebookUsers.class);
            userApi.getFacebookUsers(app.getSession().getAccessToken(), faceIds).execute(new CallableAbstract() {
                public void callback(List<TalkoModel> users, String response) {
                    afterInviteUsers(users, response);
                }
            });;
        }
    }

    private void afterInviteUsers(List<TalkoModel> users, String response) {
        List<TalkoModel> currentUsers = adapter.getUsers();
        if (currentUsers==null){
            currentUsers = new ArrayList<TalkoModel>();
        } else {
            users.removeAll(currentUsers);
        }

        currentUsers.addAll(users);
        adapter.setUsers(currentUsers);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}

