package com.aoj.joyin.fragments.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aoj.joyin.R;
import com.aoj.joyin.model.Chat;
import com.aoj.joyin.model.ChatRowUser;
import com.aoj.joyin.model.TalkoModel;

import java.util.List;

import pl.polidea.webimageview.WebImageView;

/**
 * Created by ohad on 14-11-20.
 */
public class ChatsAdapter extends BaseAdapter {

    private Context mContext;

    private List<TalkoModel> chats;

    public ChatsAdapter(Context context, List<TalkoModel> chats) {
        super();
        this.mContext = context;
        this.chats = chats;
    }

    public List<TalkoModel> getNotifications() {
        return chats;
    }

    public void setChats(List<TalkoModel> notifications) {
        this.chats = notifications;
    }


    @Override
    public int getCount() {
        if (chats==null){
            return 0;
        } else {
            return chats.size();
        }
    }

    @Override
    public Object getItem(int i) {
        return chats.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        Chat chat = (Chat) chats.get(i);

        ViewHolder holder = new ViewHolder();
        convertView = LayoutInflater.from(mContext).inflate(R.layout.chat_list_view_item, parent, false);
        holder.text = (TextView) convertView.findViewById(R.id.userFirstName);
        holder.userPic = (ImageView) convertView.findViewById(R.id.userChatPic);
        WebImageView myImage = (WebImageView)holder.userPic;

        ChatRowUser user = chat.getUser();
        myImage.setImageURL(user.getPicURL());
        holder.text.setText(user.getFirstName());
        convertView.setTag(holder);
        return convertView;
    }

    private class ViewHolder
    {
        TextView text;
        ImageView userPic;
    }

}
