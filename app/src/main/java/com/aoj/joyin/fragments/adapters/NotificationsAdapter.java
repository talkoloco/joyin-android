package com.aoj.joyin.fragments.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aoj.joyin.R;
import com.aoj.joyin.model.Notification;
import com.aoj.joyin.model.TalkoModel;

import java.io.InputStream;
import java.util.List;

import pl.polidea.webimageview.WebImageView;

/**
 * Created by ohad on 2014-07-30.
 */
public class NotificationsAdapter extends BaseAdapter {

    private Context mContext;

    private List<TalkoModel> notifications;

    public NotificationsAdapter(Context context, List<TalkoModel> notifications) {
        super();
        this.mContext = context;
        this.notifications = notifications;
    }

    public List<TalkoModel> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<TalkoModel> notifications) {
        this.notifications = notifications;
    }


    @Override
    public int getCount() {
        if (notifications==null){
            return 0;
        } else {
            return notifications.size();
        }
    }

    @Override
    public Object getItem(int i) {
        return notifications.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        Notification notification = (Notification) notifications.get(i);

        ViewHolder holder = new ViewHolder();
        convertView = LayoutInflater.from(mContext).inflate(R.layout.notification_list_view_item, parent, false);
        if (notification.isRead()){
            convertView.setBackgroundColor(Color.WHITE);
        }
        holder.text = (TextView) convertView.findViewById(R.id.notificationText);
        holder.notificationPic = (ImageView) convertView.findViewById(R.id.notificationPic);
        WebImageView myImage = (WebImageView)holder.notificationPic;
        myImage.setImageURL(notification.getPicUrl());
        holder.text.setText(notification.getText());
        convertView.setTag(holder);
        return convertView;
    }

    private class ViewHolder
    {
        TextView text;
        ImageView notificationPic;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

}
