package com.aoj.joyin.fragments;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aoj.joyin.R;
import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.activites.AppTabContainerActivity;
import com.aoj.joyin.apis.CallableAbstract;
import com.aoj.joyin.apis.GroupsApi;
import com.aoj.joyin.hooks.UserGroupsUpdatedHook;
import com.aoj.joyin.model.Group;
import com.aoj.joyin.model.GroupMessage;
import com.aoj.joyin.model.TalkoMessage;
import com.aoj.joyin.model.TalkoModel;
import com.aoj.joyin.model.TalkoUser;
import com.aoj.joyin.services.AppLocationService;
import com.aoj.joyin.utils.Consts;

import org.apache.commons.lang.StringEscapeUtils;
import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by wodom on 21/02/14.
 */
public class ChatFragment extends Fragment implements Observer{
    private TalkoApp app;
    public WebView wv;
    private View rootView;
    private ChatFragment that = this;
    private LayoutInflater inflater;
    private boolean shouldBuiltActionBarButton = true;

    private Runnable UpdateLocation = new Runnable()
    {
        @Override
        public void run(){
            AppTabContainerActivity activity = (AppTabContainerActivity)that.getActivity();
            AppLocationService locationService = activity.getAppLocationService();
            locationService.updateUserLocation();
        }
    };

    /*public Group getActiveGroup() {
        return activeGroup;
    }

    public void setActiveGroup(Group activeGroup) {
        this.activeGroup = activeGroup;
    }*/

    //private Group activeGroup;

    private String cachedGroupName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        this.inflater = inflater;
        ActionBar actionBar = getActivity().getActionBar();

        rootView = inflater.inflate(R.layout.chat, container, false);

        Thread name = new Thread(UpdateLocation);
        name.start();

        AppTabContainerActivity activity = (AppTabContainerActivity)this.getActivity();
        app = (TalkoApp)activity.getApplication();


        this.setRetainInstance(true);
        app.getLocalCache().put("groupController",this);
        try {
            this.showContextGroup();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (shouldBuiltActionBarButton) {
            Group selectedGroup = activity.getSelectedGroup();
            if (selectedGroup != null)
                buildActionBarButton(selectedGroup);
        }

        return rootView;
    }

    public void showContextGroup() throws JSONException {
        AppTabContainerActivity active = (AppTabContainerActivity)this.getActivity();
        if(active.getIntent().getExtras()!= null && active.getIntent().getExtras().containsKey("show.group") ){
            Toast.makeText(active.getApplicationContext(),
                    "Loading chat...",
                    Toast.LENGTH_SHORT).show();
            Group search  = new Group();
            JSONObject intentGroup = new JSONObject(active.getIntent().getExtras().get("show.group").toString());
            search.setId(intentGroup.getString("groupId"));
            search.setName(intentGroup.getString("groupName"));
            search.setIsPrivate(intentGroup.getBoolean("isPrivate"));
            search.setCreatorId(intentGroup.getString("groupCreatorId"));

            active.setSelectedGroup(search);
            this.getUserGroups();
            active.getIntent().removeExtra("show.group");
        }
    }

    private void buildActionBarButton(final Group selectedGroup){
        final AppTabContainerActivity activity = (AppTabContainerActivity)this.getActivity();

        ActionBar actionBar =  activity.getActionBar();

        if (inflater==null){
            return;
        }

        shouldBuiltActionBarButton = false;

        final View customActionBarView = inflater.inflate(
                R.layout.include_nav_to_manager_button, null);
        customActionBarView.findViewById(R.id.actionbar_nav_to_manager).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager fragmentManger = getFragmentManager();
                        activity.prevFragment = 3;
                        activity.getViewPager().setCurrentItem(1, false);
                        activity.setSelectedGroup(selectedGroup);
                        activity.getGroupDetails(selectedGroup.getId());
                        String managerFragmentTag = activity.getmAdapter().makeFragmentName(activity.getViewPager().getId(), 1);
                        GroupManagerFragment groupMangerFragment = (GroupManagerFragment) fragmentManger.findFragmentByTag(managerFragmentTag);
                        groupMangerFragment.reloadUsersAndTitle();
                        groupMangerFragment.updateUserList();
                        String nearbyFragmentTag = activity.getmAdapter().makeFragmentName(activity.getViewPager().getId(), 2);
                        NearbyFragment nearbyFragment = (NearbyFragment) fragmentManger.findFragmentByTag(nearbyFragmentTag);
                        nearbyFragment.updateNearbyUsers();
                    }
                }
        );


        TextView textView = (TextView)customActionBarView.findViewById(R.id.group_name);
        if (cachedGroupName==null) {
            textView.setText(selectedGroup.getName());
        } else {
            textView.setText(cachedGroupName);
        }

        //textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
        // Show the custom action bar view and hide the normal Home icon and title.
        actionBar.setDisplayOptions(
                ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_TITLE);

        ActionBar.LayoutParams params = new
                ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT, Gravity.CENTER);


        actionBar.setCustomView(customActionBarView, params);
    }



    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            AppTabContainerActivity activity = (AppTabContainerActivity)this.getActivity();
            Group selectedGroup = activity.getSelectedGroup();
            if (selectedGroup!=null)
                buildActionBarButton(selectedGroup);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.addSubMenu(0, 0, 0, "UnFollow").setIcon(R.drawable.talko_icon);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        AppTabContainerActivity activity = (AppTabContainerActivity)this.getActivity();

        switch (item.getItemId()) {
            case 0:{
                activity.getViewPager().setCurrentItem(0, false);
                GroupsApi.groupStore.remove(GroupsApi.groupStore.indexOf(activity.getSelectedGroup()));
                ((UserGroupsUpdatedHook)app.getObservable(UserGroupsUpdatedHook.class)).setGroupsList(GroupsApi.groupStore);
                app.getObservable(UserGroupsUpdatedHook.class).notifyObservers();
                activity.getActionBar().setTitle("");
                GroupsApi groupsApi = new GroupsApi(((TalkoApp) activity.getApplication()));
                groupsApi.removeUserFromGroup(activity.getSelectedGroup().getId()).execute(new CallableAbstract() {
                    public void callback(List<TalkoModel> groupList, String response) {
                    }
                });
            }
            break;
            case R.id.profile:{
                activity.logout();
                break;
            }
            case android.R.id.home:{
                if (app.getSelectedTab().equals(TalkoApp.MEETUPS)) {
                    activity.getViewPager().setCurrentItem(0, false);
                } else {
                    activity.getViewPager().setCurrentItem(4, false);
                }
                ActionBar actionBar = activity.getActionBar();
                actionBar.setTitle("");
                actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    public void getUserGroups(final Group group) {

        AppTabContainerActivity active = (AppTabContainerActivity)this.getActivity();
        active.setSelectedGroup(group);
        active.getActionBar().setTitle(group.getName());
/*        if(active.appMenu.getItem(0).getSubMenu() == null){
           active.appMenu.addSubMenu(0, 0, 0, " UnFollow");
        }*/

        this.setRetainInstance(true);
        wv = (WebView) rootView.findViewById(R.id.webPage);
        Log.i(Consts.TAG, "reloading chat");
        wv.loadUrl("about:blank");
        wv.loadUrl("file:///android_asset/web/chat.html");
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        wv.setWebChromeClient(new WebChromeClient(){
            @Override
                public void onReceivedTitle(WebView v,String title){

                    loadMessages(group);
                }

        });

    }

    private void loadMessages(Group group){
        GroupsApi groupsApi = new GroupsApi(((TalkoApp) getActivity().getApplication()), TalkoMessage.class);
        groupsApi.getGroupMessagesByChunk(group.getId(), "100").execute(new CallableAbstract() {
            public void callback(List<TalkoModel> groupList, String response) {
                afterChatMessagesRecived(groupList, response);
            }
        });
    }



    public void getUserGroups() {
        AppTabContainerActivity active = (AppTabContainerActivity)this.getActivity();
        Group group = active.getSelectedGroup();
        getUserGroups(group);
    }

    public void addChatMessage(JSONObject msg){
        wv.loadUrl("javascript:addMessage(JSON.parse('"+msg.toString()+"'))");
        wv.loadUrl("javascript: $(\"body\").scrollTop($(document).height() );");
    }

    public void getLastMessage(){
        AppTabContainerActivity active = (AppTabContainerActivity)this.getActivity();
        Group group = active.getSelectedGroup();
        GroupsApi groupsApi = new GroupsApi(((TalkoApp) this.getActivity().getApplication()), TalkoMessage.class);
        groupsApi.getGroupMessagesByChunk(group.getId(), "1").execute(new CallableAbstract() {
            public void callback(List<TalkoModel> groupList, String response) {
                afterChatMessagesRecived(groupList, response);
            }
        });
    }

    private void afterUpdateUserList(Map<String, List<TalkoModel>> groupList, String response) {
        wv.loadUrl("javascript:updatePics("+response+")");
    }

    public void afterChatMessagesRecived(List<TalkoModel> messages, String response) {
        this.updateGridView((List<TalkoMessage>) (List<?>) messages ,response);
    }

    public void updateGridView(final List<TalkoMessage> messages, String response) {
        AppTabContainerActivity active = (AppTabContainerActivity)getActivity();
        Group group = active.getSelectedGroup();
        GroupsApi groupsApi = new GroupsApi(((TalkoApp) getActivity().getApplication()), TalkoUser.class);
        Log.i(Consts.TAG, "response from srv->"+response);
        wv.loadUrl("javascript:setOwner(\""+app.getUser().getId()+"\")");

        wv.loadUrl("javascript:showMessages('"+StringEscapeUtils.escapeJavaScript(response)+"')");

        Button btn = (Button) rootView.findViewById(R.id.sendMsg);
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Group group =  ((AppTabContainerActivity)getActivity()).getSelectedGroup();
                EditText txt = (EditText) rootView.findViewById(R.id.chatLine);
                GroupsApi groupsApi = new GroupsApi(((TalkoApp) getActivity().getApplication()), TalkoMessage.class);
                GroupMessage gm = new GroupMessage();
                gm.setChunk(1);
                gm.setDate(DateTime.now().getMillis());
                gm.setGroupId(group.getId());
                gm.setSenderId(app.getUser().getId());
                gm.setText(txt.getText().toString());
                gm.setSenderName(app.getUser().getName());
                groupsApi.sendGroupMessage(gm).execute(new CallableAbstract() {
                    public void callback(List<TalkoModel> messages, String response) {
                    }
                });
                txt.setText("");
                JSONObject msg = new JSONObject();
                try {
                    msg.put("senderId",gm.getSenderId());
                    msg.put("senderName",gm.getSenderName());
                    msg.put("text",gm.getText());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                wv.loadUrl("javascript:addMessage(JSON.parse('"+msg.toString()+"'))");
               // wv.loadUrl("javascript:updatePics()");
                wv.loadUrl("javascript: $(\"body\").scrollTop($(document).height() );");
            }
        });
        GroupsApi groupsApi2 = new GroupsApi(((TalkoApp) getActivity().getApplication()), TalkoUser.class);
        groupsApi2.getGroupMembers(group.getId()).execute(new CallableAbstract() {
            public void callback(Map<String, List<TalkoModel>> groupUsers, String response) {
                afterUpdateUserList(groupUsers, response);
            }
        });
    }


    public void selectGroup() {
        ((AppTabContainerActivity) this.getActivity()).getViewPager().setCurrentItem(1,false);
    }

    @Override
    public void update(Observable observable, Object o) {

        if (observable instanceof  UserGroupsUpdatedHook){

        }

    }

    public String getCachedGroupName() {
        return cachedGroupName;
    }

    public void setCachedGroupName(String cachedGroupName) {
        this.cachedGroupName = cachedGroupName;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
