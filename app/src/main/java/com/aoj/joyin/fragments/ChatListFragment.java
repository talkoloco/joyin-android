package com.aoj.joyin.fragments;

import android.app.ActionBar;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.aoj.joyin.R;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.activites.AppTabContainerActivity;
import com.aoj.joyin.apis.CallableAbstract;
import com.aoj.joyin.apis.ChatsApi;
import com.aoj.joyin.fragments.adapters.ChatsAdapter;
import com.aoj.joyin.model.Chat;
import com.aoj.joyin.model.Chats;
import com.aoj.joyin.model.Group;
import com.aoj.joyin.model.TalkoModel;

import java.util.List;

/**
 * Created by ohad on 2014-07-29.
 */
public class ChatListFragment extends Fragment {

    private ChatsAdapter adapter;
    private List<TalkoModel> chats;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        adapter = new ChatsAdapter(getActivity(), chats);

        View rootView = inflater.inflate(R.layout.chats, container, false);

        final AppTabContainerActivity activity = (AppTabContainerActivity)this.getActivity();


        final ListView list = (ListView)rootView.findViewById(R.id.chatsListView);
        list.setClickable(true);
        list.setLongClickable(true);

        final TalkoApp app = ((TalkoApp) activity.getApplication());

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                view.setBackgroundColor(Color.WHITE);
                final Chat chat = (Chat)list.getItemAtPosition(position);
                return false;
            }

        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setBackgroundColor(Color.WHITE);
                final Chat chat = (Chat)list.getItemAtPosition(position);
            }

        });

        list.setAdapter(adapter);
        this.setRetainInstance(true);

        return rootView;
    }

    private void navigateToGroup(Chat chat) {
        AppTabContainerActivity activity = (AppTabContainerActivity)this.getActivity();
        String groupId = chat.getGroupId();
        String groupName = chat.getGroupName();

        Group group = new Group();
        group.setId(groupId);
        group.setName(groupName);
        activity.setSelectedGroup(group);

        FragmentManager fragmentManger = activity.getSupportFragmentManager();
        String fragmentTag = activity.getmAdapter().makeFragmentName(activity.getViewPager().getId(), 3);
        ChatFragment chatFragment = (ChatFragment) fragmentManger.findFragmentByTag(fragmentTag);
        WebView wv = chatFragment.wv;
        if (wv!=null) {
            wv.loadUrl("about:blank");
            wv = null;
        }
        activity.getViewPager().setCurrentItem(3,false);

        chatFragment.getUserGroups();
        chatFragment.setCachedGroupName(null);

        // Get tracker.
        Tracker t = ((TalkoApp) activity.getApplication()).getTracker(
                TalkoApp.TrackerName.APP_TRACKER);
        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory("chat")
                .setAction("load.chat")
                .setLabel("chat.label")
                .build());
    }

    public void updateChats(){
        TalkoApp app = ((TalkoApp) getActivity().getApplication());
        String userGroupId = app.getUser().getActiveGroupId();
        AppTabContainerActivity active = (AppTabContainerActivity)this.getActivity();
        ChatsApi chatsApi = new ChatsApi(app, Chats.class);
        chatsApi.getUserChats(userGroupId).execute(new CallableAbstract() {
            public void callback(List<TalkoModel> chats, String response){
                afterUpdateChats(chats, response);
            }
        });
    }

    private void afterUpdateChats(List<TalkoModel> chats, String response) {
        this.chats = chats;
        adapter.setChats(chats);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        AppTabContainerActivity activity = (AppTabContainerActivity)this.getActivity();
        if (activity==null) return;
        ActionBar actionBar = activity.getActionBar();
        if (isVisibleToUser){
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        } else {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        }
    }



    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume(){
        super.onResume();
    }
}
