package com.aoj.joyin.fragments.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import pl.polidea.webimageview.WebImageView;

import com.aoj.joyin.R;
import com.aoj.joyin.model.TalkoModel;
import com.aoj.joyin.model.TalkoUser;

import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ohad on 2/20/2014.
 */
public class NearbyFragmentAdapter extends BaseAdapter {
    private Context mContext;

    public void setUsers(List<TalkoModel> users) {
        this.users = users;
    }

    public Set<TalkoUser> getSelectedUsers() {
        return selectedUsers;
    }

    private Set<TalkoUser> selectedUsers;

    public List<TalkoModel> getUsers() {
        return users;
    }

    private List<TalkoModel> users;

    public NearbyFragmentAdapter(Context context, List<TalkoModel> users) {
        super();
        this.mContext = context;
        this.users = users;
        this.selectedUsers = new HashSet<TalkoUser>();

    }


    @Override
    public int getCount() {
        if (users==null){
            return 0;
        } else {
            return users.size();
        }
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        TalkoUser user = (TalkoUser) users.get(i);
        Log.w("Talko", user.getName());

        ViewHolder holder = new ViewHolder();
        convertView = LayoutInflater.from(mContext).inflate(R.layout.nearby_list_view_item, parent, false);
        holder.userName = (TextView) convertView.findViewById(R.id.username);
        holder.userPic = (ImageView) convertView.findViewById(R.id.userPic);
        holder.checkBok = (CheckBox) convertView.findViewById(R.id.checkbox);
        holder.checkBok.setTag(user);
        holder.checkBok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View cView) {
                CheckBox checkView = (CheckBox)cView;
                TalkoUser user = (TalkoUser) checkView.getTag();
                if (checkView.isChecked()) {
                    selectedUsers.add(user);
                } else {
                    selectedUsers.remove(user);
                }
            }
        });

        WebImageView myImage = (WebImageView)holder.userPic;
        myImage.setImageURL(user.getPicUrl());
        convertView.setTag(holder);
        holder.userName.setText(user.getName());
        return convertView;
    }

    private class ViewHolder
    {
        TextView userName;
        ImageView userPic;
        CheckBox checkBok;

    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    public void onItemClick(AdapterView<?> parent, View view,
                            int position, long id) {

    }

}

