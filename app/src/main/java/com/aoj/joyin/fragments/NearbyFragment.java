package com.aoj.joyin.fragments;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andtinder.model.CardModel;
import com.andtinder.model.Orientations;
import com.andtinder.view.CardContainer;
import com.andtinder.view.SimpleCardStackAdapter;
import com.aoj.joyin.R;
import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.activites.AppTabContainerActivity;
import com.aoj.joyin.apis.CallableAbstract;
import com.aoj.joyin.apis.UserApi;
import com.aoj.joyin.fragments.adapters.NearbyFragmentAdapter;
import com.aoj.joyin.model.Group;
import com.aoj.joyin.model.TalkoUsers;
import com.aoj.joyin.model.TalkoModel;
import com.aoj.joyin.model.TalkoUser;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pl.polidea.webimageview.WebImageView;

/**
 * Created by ohad on 2/20/2014.
 */
public class NearbyFragment extends DialogFragment {


    private int mNum;
    private NearbyFragmentAdapter adapter;

    private static List<TalkoModel> users;


    static NearbyFragment newInstance(int num) {
        NearbyFragment f = new NearbyFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, android.R.style.Theme_Holo_Light);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.nearby_fragment_dialog, container, false);

      /*  adapter = new NearbyFragmentAdapter(getActivity(), users);
        ListView list = (ListView)rootView.findViewById(R.id.nearbyListView);
        list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ViewGroup row = (ViewGroup)view;
                CheckBox check = (CheckBox) row.findViewById(R.id.checkbox);
                check.toggle();
            }
        });

        list.setAdapter(adapter);

        final NearbyFragment that = this;

        Button cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                that.dismiss();
            }
        });

        Button okBtn = (Button)rootView.findViewById(R.id.okBtn);
        okBtn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                inviteNearbyUsers();
            }
        });

        TextView noOneNearby = (TextView)rootView.findViewById(R.id.noOneNearby);
        if (users!=null && users.size()>0){
            noOneNearby.setVisibility(TextView.INVISIBLE);
        } else {
            noOneNearby.setVisibility(TextView.VISIBLE);
        }

        adapter.notifyDataSetChanged();*/
       CardContainer mCardContainer = (CardContainer) rootView.findViewById(R.id.layoutview);
        /*TableRow tlRow = (TableRow)  rootView.findViewById(R.id.commonInterests);
        WebImageView img  = new WebImageView(this.getActivity());

        tlRow.addView(img);
        img.setImageURL("http://upload.wikimedia.org/wikipedia/en/b/bc/Wiki.png");*/
    //    mCardContainer.setGravity(Gravity.BOTTOM);
        Resources r = getResources();
        SimpleCardStackAdapter adapter = new SimpleCardStackAdapter(getActivity());
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.id.userPic);
        mCardContainer.setOrientation(Orientations.Orientation.Ordered);
        adapter.add(new CardModel("Title1", "Description goes here",  "http://hdwallpaperfresh.com/wp-content/uploads/2013/12/3D-Sexy-Girl-Tattoos-Wallpaper.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg"));
        adapter.add(new CardModel("Title1", "Description goes here",  "http://hdwallpaperfresh.com/wp-content/uploads/2013/12/3D-Sexy-Girl-Tattoos-Wallpaper.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg"));
        adapter.add(new CardModel("Title1", "Description goes here",  "http://hdwallpaperfresh.com/wp-content/uploads/2013/12/3D-Sexy-Girl-Tattoos-Wallpaper.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg","http://needfulthngz.com/coppermine/albums/userpics/Sexy-Girl-98.jpg"));

        mCardContainer.setAdapter(adapter);
        mCardContainer.setGravity(Gravity.TOP);
        return rootView;
    }

    private void inviteNearbyUsers() {

        TalkoApp app = ((TalkoApp) this.getActivity().getApplication());
        AppTabContainerActivity active = (AppTabContainerActivity)this.getActivity();
        Group group = active.getSelectedGroup();

        Set<TalkoUser> talkoUsers = null;

        if (group!=null) {
            talkoUsers = validateUsers(group.getMembers(), adapter.getSelectedUsers());
        } else {
            talkoUsers = adapter.getSelectedUsers();
        }

        if (talkoUsers==null){
            this.dismiss();
            return;
        }

        Set<String> facebookIds = new HashSet<String>(talkoUsers.size());
        for (TalkoUser user: talkoUsers){
            facebookIds.add(user.getFaceId());
        }

        UserApi userApi = new UserApi(app, TalkoUsers.class);
        if (group!=null) {
            userApi.inviteFacebookUsers(group, facebookIds).execute(new CallableAbstract() {
                public void callback(List<TalkoModel> users, String response) {
                    afterInviteUsers(users, response);
                }
            });
        } else {
            userApi.getTalkoUsersByFacebookIds(app.getSession().getAccessToken(), facebookIds).execute(new CallableAbstract() {
                public void callback(List<TalkoModel> users, String response) {
                    afterInviteUsers(users, response);
                }
            });
        }
    }

    private Set<TalkoUser> validateUsers(Set<String> members, Set<TalkoUser> selectedUsers) {
        if (selectedUsers==null || selectedUsers.size()==0){
            return null;
        }
        for (TalkoUser user : selectedUsers){
            if (members!=null && members.contains(user.getId())){
                selectedUsers.remove(user);
            }
        }
        if (selectedUsers==null || selectedUsers.size()==0){
            return null;
        }
        return selectedUsers;
    }

    private void afterInviteUsers(List<TalkoModel> users, String response) {
        AppTabContainerActivity tabActivity = (AppTabContainerActivity)this.getActivity();
        FragmentManager fragmentManger =  tabActivity.getSupportFragmentManager();
        String fragmentTag = tabActivity.getmAdapter().makeFragmentName(tabActivity.getViewPager().getId(), 1);
        GroupManagerFragment groupManagerFragment = (GroupManagerFragment)fragmentManger.findFragmentByTag(fragmentTag);
        Set<String> facebookIds = groupManagerFragment.getFacebookIds();
        for (TalkoModel model : users){
            TalkoUser user = (TalkoUser)model;
            String facebookId = user.getFaceId();
            if (facebookIds==null){
                facebookIds = new HashSet<String>(users.size());
            }
            facebookIds.add(facebookId);
        }

        groupManagerFragment.setFacebookIds(facebookIds);
        groupManagerFragment.reloadUsers(users);
        this.dismiss();
    }

    public void updateNearbyUsers() {

        AppTabContainerActivity active = (AppTabContainerActivity)this.getActivity();
        Group group = active.getSelectedGroup();
        TalkoApp app = (TalkoApp) getActivity().getApplication();
        UserApi userApi = new UserApi(app, TalkoUsers.class);
        String token = app.getSession().getAccessToken();
        userApi.getUsersNearBy(token).execute(new CallableAbstract() {
            public void callback(List<TalkoModel> nearbyUsers, String response) {
                afterGettingUserList(nearbyUsers, response);
            }
        });
    }

    private void afterGettingUserList(List<TalkoModel> nearbyUsers, String response) {
        users = nearbyUsers;
    }

    @Override
    public void onDismiss(android.content.DialogInterface dialog){
        super.onDismiss(dialog);


    }
}