package com.aoj.joyin.fragments.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aoj.joyin.R;
import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.activites.AppTabContainerActivity;
import com.aoj.joyin.apis.CallableAbstract;
import com.aoj.joyin.apis.GroupsApi;
import com.aoj.joyin.model.Group;
import com.aoj.joyin.model.TalkoModel;
import com.aoj.joyin.model.TalkoUser;
import com.aoj.joyin.views.AcceptButton;

import java.io.InputStream;
import java.util.List;

import pl.polidea.webimageview.WebImageView;

/**
 * Created by ohad on 2/15/2014.
 */
public class ManageGroupListAdapter extends BaseAdapter {
    private Context mContext;

    public void setUsers(List<TalkoModel> users) {

        this.users = users;
    }

    public List<TalkoModel> getUsers() {
        return users;
    }

    private List<TalkoModel> users;

    public ManageGroupListAdapter(Context context, List<TalkoModel> users) {
        super();
        this.mContext = context;
        this.users = users;
    }


    @Override
    public int getCount() {
        if (users==null){
            return 0;
        } else {
            return users.size();
        }
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    private View.OnClickListener btnListener = new View.OnClickListener()
    {

        public void onClick(View v){
            AppTabContainerActivity activity = ((AppTabContainerActivity)mContext);
            Group group = activity.getSelectedGroup();
            final AcceptButton acceptButton = (AcceptButton)v;
            String userId = acceptButton.getUserId();
            TalkoApp app = ((TalkoApp) activity.getApplication());
            GroupsApi groupsApi = new GroupsApi(app, Group.class);
            groupsApi.approveUserAtGroup(userId, group.getId()).execute(new CallableAbstract() {
                public void callback(TalkoModel group, String response) {
                    acceptButton.setVisibility(View.INVISIBLE);
                    acceptButton.status.setText("(Member)");
                }
            });
        }

    };

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        TalkoUser user = (TalkoUser) users.get(i);
        Log.w("Talko", user.getName());
        ViewHolder holder;
        holder = new ViewHolder();
        convertView = LayoutInflater.from(mContext).inflate(R.layout.manger_list_view_item, parent, false);
        holder.userName = (TextView) convertView.findViewById(R.id.username);
        holder.status = (TextView) convertView.findViewById(R.id.status);
        holder.userPic = (ImageView) convertView.findViewById(R.id.userPic);
        holder.acceptBtn = (AcceptButton)convertView.findViewById(R.id.accpetBtn);
        holder.acceptBtn.setUserId(user.getId());
        holder.acceptBtn.status = holder.status;
        holder.acceptBtn.setOnClickListener(btnListener);
        WebImageView myImage = (WebImageView)holder.userPic;
        myImage.setImageURL(user.getPicUrl());
        convertView.setTag(holder);


        holder.userName.setText(user.getName());
        String type = user.getType();

        AppTabContainerActivity activity = ((AppTabContainerActivity)mContext);
        Group group = activity.getSelectedGroup();

        if ("regular".equals(type)) {
            holder.acceptBtn.setVisibility(Button.INVISIBLE);
            if (group!=null && group.getCreatorId()!=null && group.getCreatorId().equals(user.getId())){
                holder.status.setText("(Admin)");
            } else {
                holder.status.setText("(Member)");
            }
        } else if ("invitedUser".equals(type)){
            holder.status.setText("(Invited)");
            holder.acceptBtn.setVisibility(Button.INVISIBLE);
        } else {
            holder.status.setText("(Pending)");
            holder.acceptBtn.setVisibility(Button.VISIBLE);
        }

        return convertView;
    }

    private class ViewHolder
    {
        TextView userName;
        TextView status;
        ImageView userPic;
        AcceptButton acceptBtn;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

}
