package com.aoj.joyin.services;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.activites.AppTabContainerActivity;
import com.aoj.joyin.fragments.ChatFragment;
import com.aoj.joyin.model.Group;

import org.json.JSONObject;

/**
 * Created by ohad on 2013-12-05.
 */

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // Explicitly specify that GcmIntentService will handle the intent.
        ComponentName comp = new ComponentName(context.getPackageName(),
                GcmIntentService.class.getName());
        // Start the service, keeping the device awake while it is launching.
        try {

            if(intent.getExtras().get("message") == null){
                startWakefulService(context, (intent.setComponent(comp)));
                return;
            }

            JSONObject message = new JSONObject(intent.getExtras().get("message").toString());
            setResultCode(Activity.RESULT_OK);
            TalkoApp app = (TalkoApp) context.getApplicationContext();


            ChatFragment chatFragmentRef = (ChatFragment)app.getLocalCache().get("groupController");
            if(chatFragmentRef == null){
                startWakefulService(context, (intent.setComponent(comp)));
                return;
            }

            AppTabContainerActivity active = (AppTabContainerActivity)chatFragmentRef.getActivity();
            Group group = active.getSelectedGroup();
            Vibrator vibe = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE) ;
            if(!(group == null) && (group.getId().equals(message.get("groupId")) && !(app.getUser().getId()
                    .equals(message.get("senderId"))))){
                chatFragmentRef.addChatMessage(message);
                vibe.vibrate(600);
            }else if((group == null )||  !(app.getUser().getId().equals(message.get("senderId")))){
                startWakefulService(context, (intent.setComponent(comp)));
                vibe.vibrate(600);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}