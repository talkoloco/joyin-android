package com.aoj.joyin.services;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.activites.AppTabContainerActivity;
import com.aoj.joyin.apis.CallableAbstract;
import com.aoj.joyin.apis.UserApi;
import com.aoj.joyin.model.TalkoModel;

public class AppLocationService extends Service implements LocationListener {

    protected LocationManager locationManager;
    //private Location latestLocation;
    private TalkoApp app;
    private static final long MIN_DISTANCE_FOR_UPDATE = 10;
    private static final long MIN_TIME_FOR_UPDATE = 1000 * 60 * 2;
    private Context context;

    public AppLocationService(Context serviceContext) {
        this.context = serviceContext;
        locationManager = (LocationManager) serviceContext
                .getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);
    }

    private Location getLastLocationByProvider(String provider) {
        return locationManager.getLastKnownLocation(provider);
    }

    public void updateUserLocation(){
        Location location = getLatestUserLocation();
        updateUserLocationOnApp(location);
    }

    public Location getLatestUserLocation(){
        return getLatestUserLocation(null);
    }

    public Location getLatestUserLocation(Location nwLocation){

        if (nwLocation==null){
            nwLocation = getLastLocationByProvider(LocationManager.NETWORK_PROVIDER);
        }

        Location gpsLocation = getLastLocationByProvider(LocationManager.GPS_PROVIDER);
        if (nwLocation==null && gpsLocation==null){
            return null;
        }

        if (nwLocation!=null && gpsLocation==null){
            return nwLocation;
        }

        if (nwLocation==null && gpsLocation!=null){
            return gpsLocation;
        }

        boolean isNWBetterLocation = isBetterLocation(gpsLocation, nwLocation);

        Location latestLocation = null;

        if (isNWBetterLocation){
            latestLocation = nwLocation;
        } else {
            latestLocation = gpsLocation;
        }

        return latestLocation;
    }


        @Override
    public void onLocationChanged(Location lastLocation) {
        Location latestLocation = getLatestUserLocation(lastLocation);
        updateUserLocationOnApp(latestLocation);
    }

    private void updateUserLocationOnApp(Location location){
        AppTabContainerActivity activity = (AppTabContainerActivity)context;
        app = (TalkoApp)activity.getApplication();
        if (app.getUser()!=null){
            app.getUser().setLocation(location);

            UserApi userApi = new UserApi(app);
            try {
                userApi.saveUserLocation().execute(new CallableAbstract() {
                    public void callback(TalkoModel model, String response) {}
                });
            } catch (Exception e){
                Log.i(this.getClass().getName(), "Error while saving user location.");
            }
        };


    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
        if (LocationManager.GPS_PROVIDER.equals(provider)){
            Location latestLocation = getLatestUserLocation();
            updateUserLocationOnApp(latestLocation);
        } else if (LocationManager.GPS_PROVIDER.equals(provider)){
            Location latestLocation = getLatestUserLocation();
            updateUserLocationOnApp(latestLocation);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}