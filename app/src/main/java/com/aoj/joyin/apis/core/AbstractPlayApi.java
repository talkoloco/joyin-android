package com.aoj.joyin.apis.core;

import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.apis.ApiConnector;

import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by I071873 on 1/4/14.
 */
public abstract class AbstractPlayApi implements PlayApi {

    public AbstractPlayApi(){}

    protected ApiConnector connector;
    protected Callable func;
    protected TalkoApp app;
    protected Class responseType;

    public AbstractPlayApi(TalkoApp app,Class responseType){
        this.connector = app.getApiConnector();
        this.app = app;
        this.responseType= responseType;
    }

    protected void setPath(String path){


        this.connector.path = path;
    }

    protected void setPath(String path, HashMap<String,String> params){

        for( Map.Entry i : params.entrySet()){
            path= path.replaceAll(i.getKey().toString(),i.getValue().toString());
        }

        this.connector.path = path;
    }


    protected void set(String key, StringEntity value){
        try{
            JSONObject params = connector.getJsonParams();

            params.put(key, value.getContent().toString());

        }catch (Exception e){

        }

    }

    protected void set(String key, JSONObject value){
        try{
            JSONObject params = connector.getJsonParams();
            params.put(key, value);
        }catch (JSONException e){

        }
    }

    protected void set(String key, JSONArray value){
        try{
            JSONObject params = connector.getJsonParams();
            params.put(key, value);
        }catch (JSONException e){

        }
    }



    protected void set(String key,String value){
        try{
            JSONObject params = connector.getJsonParams();
            params.put(key, value);
        }catch (JSONException e){

        }
    }

    protected void set(String key,Long value){
        try{
            JSONObject params = connector.getJsonParams();
            params.put(key, value);
        }catch (JSONException e){

        }
    }

    protected void set(String key,Integer value){
        try{
            JSONObject params = connector.getJsonParams();
            params.put(key, value);
        }catch (JSONException e){

        }
    }

    protected void set(String key,Double value){
        try{
            JSONObject params = connector.getJsonParams();
            params.put(key, value);
        }catch (JSONException e){

        }
    }

    protected void setRequestType(RequestType reqType){
        this.connector.methodType = reqType;
    }


    public void execute(Callable func){
        this.func = func;
        connector.execute(func,this);
    }
}
