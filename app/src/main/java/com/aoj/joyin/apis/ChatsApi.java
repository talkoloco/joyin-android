package com.aoj.joyin.apis;

import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.apis.core.AbstractPlayApi;
import com.aoj.joyin.apis.core.PlayApi;
import com.aoj.joyin.apis.core.RequestType;
import com.aoj.joyin.model.Chats;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by ohad on 14-11-20.
 */
public class ChatsApi extends AbstractPlayApi {

    private final String GET_USER_CHATS = "me/:userGroupId/chats";

    public ChatsApi(TalkoApp app){
        super(app,Chats.class);
    }

    public ChatsApi(TalkoApp app,Class className){
        super(app,className);
    }

    public void configConnector(String userId){
        try{
            JSONObject params = connector.getJsonParams();
            params.put("userId", userId);
        } catch(Exception e){

        }
    }

    @Override
    public void processEntity(String responseBody) {

    }

    public PlayApi getUserChats(String userGroupId) {
        this.setRequestType(RequestType.POST);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(":userGroupId", userGroupId);
        this.setPath(GET_USER_CHATS, params);
        return this;
    }
}
