package com.aoj.joyin.apis;

import com.aoj.joyin.apis.core.Callable;
import com.aoj.joyin.model.TalkoModel;

import java.util.List;
import java.util.Map;

/**
 * Created by I071873 on 1/4/14.
 */
public abstract class CallableAbstract implements Callable {
    @Override
    public void callback() {

    }

    @Override
    public void callback(TalkoModel model, String response) {

    }

    @Override
    public void callback(List<TalkoModel> modelList, String response) {

    }

    @Override
    public void callback(Map<String, List<TalkoModel>> modelMap, String response){

    }



}
