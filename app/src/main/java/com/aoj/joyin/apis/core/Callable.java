package com.aoj.joyin.apis.core;

import com.aoj.joyin.model.TalkoModel;

import java.util.List;
import java.util.Map;

/**
 * Created by I071873 on 1/4/14.
 */
public interface Callable {

    public void callback();

    public void callback(TalkoModel model,String response);

    public void callback(List<TalkoModel> modelList,String response);

    public void callback(Map<String, List<TalkoModel>> modelMap, String response);

}
