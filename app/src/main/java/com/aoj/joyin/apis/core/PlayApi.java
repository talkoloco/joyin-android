package com.aoj.joyin.apis.core;


public interface PlayApi {
    public void processEntity(String responseBody);
    public void execute(Callable func);
}
