package com.aoj.joyin.apis.core;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.aoj.joyin.model.TalkoUser;
import com.aoj.joyin.utils.Consts;
import com.aoj.joyin.utils.JsonConverter;

import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;


/**
 * Created by I071873 on 1/4/14.
 */
public abstract class AbstractApi {

    public AsyncHttpClient getClient() {
        return client;
    }

    public void setClient(AsyncHttpClient client) {
        this.client = client;
    }

    protected AsyncHttpClient client;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    protected Context context;

    public JSONObject getJsonParams() {
        return jsonParams;
    }

    public String path;

    public void setJsonParams(JSONObject jsonParams) {
        this.jsonParams = jsonParams;
    }

    public RequestParams getRequestParams(){

        RequestParams params = new RequestParams();
        try{
        Iterator<?> keys = this.getJsonParams().keys();
        while( keys.hasNext() ){
            String key = (String)keys.next();
            if( this.getJsonParams().get(key) instanceof String ){
                params.put(key,this.getJsonParams().getString(key));
            }
        }

        }catch(Exception e){

        }
     return params;
    }

    protected JSONObject jsonParams = new JSONObject();

    protected RequestType methodType = RequestType.POST;


    public void execute (final Callable func,final PlayApi ref){
        try {
            ByteArrayEntity entity = new ByteArrayEntity(this.getJsonParams().toString().getBytes("UTF-8"));
            JsonHttpResponseHandler rh = new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, org.apache.http.Header[] headers, java.lang.String responseBody) {
                    try {
                        ref.processEntity(responseBody);
                    } catch (Exception e) {
                        Log.e("dsa", e.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, org.apache.http.Header[] headers, java.lang.String responseBody, java.lang.Throwable e) {
                    Log.e("error ..",e.toString());
                }
            };
            RequestType selectedType = RequestType.valueOf(methodType.toString());

            switch (selectedType) {
               case POST:
                    client.post(context, Consts.URL + this.path, entity, "application/json", rh);
               break;
                case GET:
                    client.get(context, Consts.URL + this.path,  this.getRequestParams(), rh);
                break;
                case PUT:
                    client.put(context, Consts.URL + this.path, entity, "application/json", rh);
                break;
                default:
                    throw new IllegalArgumentException("Invalid day of the week: " + methodType);
            }


        }catch(Exception e){
            Log.e("dsa",e.toString());
        }

    }

    private void parseUserFromResponse(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            TalkoUser talkoUser = JsonConverter.convertFromJsonToUser(jsonObject);
 /*           SharedPreferences preferences = this.getSharedPreferences("TalkoPreferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("userId", talkoUser.getId());
            editor.putString("userName", talkoUser.getName());
*/
/*            editor.commit();*/
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

}
