package com.aoj.joyin.apis.core;

/**
 * Created by I071873 on 1/11/14.
 */
public enum RequestType {
    POST, GET, PUT, DELETE
}