package com.aoj.joyin.apis;

import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.apis.core.AbstractPlayApi;
import com.aoj.joyin.apis.core.PlayApi;
import com.aoj.joyin.apis.core.RequestType;
import com.aoj.joyin.model.Group;
import com.aoj.joyin.model.TalkoFacebookUsers;
import com.aoj.joyin.model.TalkoUsers;
import com.aoj.joyin.model.TalkoModel;
import com.aoj.joyin.model.TalkoUser;
import com.aoj.joyin.utils.JsonConverter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class UserApi extends AbstractPlayApi {

    private static final  String LOGIN_PATH="login/facebookWithDevice";
    private final String SAVE_USER_LOCATION ="saveUserLocation";
    private final String GET_USERS_NEAR_BY ="invite/nearBy";
    private final String INVITE_FACEBOOK_USERS ="group/:groupId/invite";
    private final String GET_TALKO_USERS_BY_FACEBOOK_IDS ="invite/getTalkoUsersByFaceIds";
    private final String GET_FACEBOOK_USERS ="invite/getFacebookUsersByIds";


    public UserApi(TalkoApp app){
        super(app,TalkoUser.class);
    }

    public UserApi(TalkoApp app,Class className){
        super(app,className);
    }

    public PlayApi doLogin(String token, String deviceToken, String deviceType) {
        this.setRequestType(RequestType.POST);
        this.setPath(LOGIN_PATH);
        if (deviceToken==null || "".equals(deviceToken)){
            deviceToken = "no-device-token";
        }
        this.set("deviceToken", deviceToken);
        this.set("token",token);
        this.set("deviceType",deviceType);

        return this;
    }

    public PlayApi getTalkoUsersByFacebookIds(String token, Set<String> facebookIds){
        this.setPath(GET_TALKO_USERS_BY_FACEBOOK_IDS);
        this.set("token",token);
        this.set("facebookIds", new JSONArray(facebookIds));
        return this;
    }

    public PlayApi getFacebookUsers(String token, Set<String> facebookIds){
        this.setPath(GET_FACEBOOK_USERS);
        this.set("token",token);
        this.set("facebookIds", new JSONArray(facebookIds));
        return this;
    }

    public PlayApi saveUserLocation(){
        this.setRequestType(RequestType.POST);
        this.setPath(SAVE_USER_LOCATION);

        String longitude = null;
        String latitude = null;

        try {
            longitude = Double.toString(app.getUser().getLocation().getLongitude());
            latitude = Double.toString(app.getUser().getLocation().getLatitude());
        } catch (Exception e){
            throw new RuntimeException(e);
        }

        this.set("longitude", longitude);
        this.set("latitude",latitude);
        this.set("userId", app.getUser().getId());
        return this;
    }

    public PlayApi getUsersNearBy(String token) {
        this.setRequestType(RequestType.POST);
        this.setPath(GET_USERS_NEAR_BY);
        this.set("token",token);
        return this;
    }


    public PlayApi inviteFacebookUsers(Group group, Set<String> faceIds) {

        if (faceIds==null || faceIds.size()==0){
            throw new RuntimeException("facebook ids cannot be empty when inviting");
        }

        if (group==null){
            throw new RuntimeException("group cannot be null when inviting");
        }

        this.setRequestType(RequestType.POST);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(":groupId", group.getId());
        this.setPath(INVITE_FACEBOOK_USERS, params);

        List<JSONObject> fIds = new ArrayList<JSONObject>(faceIds.size());

        for (String faceId : faceIds){
            JSONObject inviteeJson = createInviteeJson(faceId, group);
            fIds.add(inviteeJson);
        }

        JSONArray inviteesArray = new JSONArray(fIds);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(group.getId(), inviteesArray);
        } catch (JSONException e){
            throw new RuntimeException(e);
        }
        this.set("invitees",jsonObject);
        return this;
    }

    private JSONObject createInviteeJson(String faceId, Group group){
        JSONObject invitee = new JSONObject();
        try{
            invitee.put("groupId", group.getId());
            invitee.put("id", "111111111111111111111111");
            invitee.put("name", "NA");
            invitee.put("inviteType", "Friend");
            invitee.put("groupTitle",group.getName());
            invitee.put("faceId", faceId);
            invitee.put("status", "Invited");
            invitee.put("picUrl", "NA");
        } catch (JSONException e){
            throw new RuntimeException(e);
        }
        return invitee;
    }

    public void processEntity(String apiResponse){
        try{

            if ("[]".equals(apiResponse)){
                List<TalkoModel> users = null;
                func.callback(users, apiResponse);
            } else if ("{}".equals(apiResponse)){
                TalkoModel user = null;
                func.callback(user, apiResponse);

            }

            if(responseType.equals(TalkoUser.class)){
                TalkoUser user = JsonConverter.convertFromJsonToUser(new JSONObject(apiResponse));
                 func.callback(user, apiResponse);

            } else if(responseType.equals(TalkoUsers.class)){
                JSONArray users =  new JSONArray(apiResponse);
                List<TalkoModel> tUsers = new ArrayList<TalkoModel>(users.length());

                for (int i = 0; i < users.length(); i++) {
                    JSONObject user = users.getJSONObject(i);
                    TalkoUser tUser = JsonConverter.convertFromJsonToUser(user);
                    tUsers.add(tUser);

                }
                func.callback(tUsers, apiResponse);
            }  else if(responseType.equals(TalkoFacebookUsers.class)){
                JSONArray users =  new JSONArray(apiResponse);
                List<TalkoModel> tUsers = new ArrayList<TalkoModel>(users.length());

                for (int i = 0; i < users.length(); i++) {
                    JSONObject user = users.getJSONObject(i);
                    TalkoUser tUser = JsonConverter.convertFromFacebookJsonToUser(user);
                    tUsers.add(tUser);

                }
                func.callback(tUsers, apiResponse);
            }




        } catch(Exception e){
            throw new RuntimeException(e);
        }
    }


}