package com.aoj.joyin.apis;

import android.location.Address;
import android.location.Geocoder;

import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.apis.core.AbstractPlayApi;
import com.aoj.joyin.apis.core.PlayApi;
import com.aoj.joyin.apis.core.RequestType;
import com.aoj.joyin.model.Group;
import com.aoj.joyin.model.GroupMessage;
import com.aoj.joyin.model.Groups;
import com.aoj.joyin.model.TalkoMessage;
import com.aoj.joyin.model.TalkoModel;
import com.aoj.joyin.model.TalkoUser;
import com.aoj.joyin.utils.JsonConverter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by I071873 on 1/11/14.
 */
public class GroupsApi extends AbstractPlayApi {

    public static List<TalkoModel> groupStore = null;

    private final String GET_GROUPS="userGroups";
    private final String GET_GROUPS_NEARBY="groupsByCords";
    private final String ADD_USER_TO_GROUP= "group/:groupId/add/user/:userId";
    private final String JOIN_TO_GROUP ="group/:groupId/join/user/:userId";
    private final String APPROVE_USER_JOIN ="group/:groupId/approve/user/:userId";
    private final String GET_GROUP_MESSAGES_BY_CHUNK ="getGroupMessagesByChunk";
    private final String CREATE_GROUP ="createGroup";
    private final String CREATE_PRIVATE_GROUP = "createPrivateGroupWithPartner";
    private final String GET_GROUP_MEMBERS ="group/:groupId/members";
    private final String UPDATE_GROUP_NAME = "group/:groupId/name";
    private final String SEND_GROUP_MESSAGE = "newGroupMessage";
    private final String REMOVE_USER_FROM_GROUP= "removeUserFromGroup";
    private final String GROUP_MESSAGE ="newGroupMessage";
    private final String GET_GROUP= "group/:groupId/details";



    public GroupsApi(TalkoApp app){
        super(app,Groups.class);
    }

    public GroupsApi(TalkoApp app,Class className){
        super(app,className);
    }

    public void configConnector(String userId){
        try{
            JSONObject params = connector.getJsonParams();
            params.put("userId", userId);
        } catch(Exception e){

        }
    }

    public PlayApi approveUserAtGroup(String userId , String groupId){
        this.setRequestType(RequestType.PUT);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(":groupId", groupId);
        params.put(":userId", userId);
        this.setPath(APPROVE_USER_JOIN, params);
        return this;
    }

    public PlayApi getGroup(String groupId){
        this.setRequestType(RequestType.POST);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(":groupId", groupId);
        this.setPath(GET_GROUP, params);
        return this;
    }


    public PlayApi removeUserFromGroup( String groupId){
        this.setRequestType(RequestType.POST);
        this.set("userId",app.getUser().getId());
        this.set("groupId",groupId);
        this.setPath(REMOVE_USER_FROM_GROUP);
        return this;
    }

    public PlayApi updateGroupName(String groupId, String name){
        this.setRequestType(RequestType.POST);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(":groupId", groupId);
        this.setPath(UPDATE_GROUP_NAME, params);
        this.set("groupId",groupId);
        this.set("groupName",name);
        return this;
    }

    public PlayApi getGroups(){
        this.setPath(GET_GROUPS);
        this.set("userId",app.getUser().getId());
        return this;
    }

    public PlayApi getGroupsNearBy(){
        this.setPath(GET_GROUPS_NEARBY);
        this.set("longitude", Double.toString(app.getUser().getLocation().getLongitude()));
        this.set("latitude",Double.toString(app.getUser().getLocation().getLatitude()));
        return this;
    }

    public PlayApi createPrivateGroup(String groupName, String partnerId){
        Geocoder geocoder;
        List<Address> addresses = new  ArrayList <Address>();
        geocoder = new Geocoder(app,Locale.getDefault());
        try{
            addresses = geocoder.getFromLocation(app.getUser().getLocation().getLatitude(), app.getUser().getLocation().getLongitude(), 1);
        }catch(Exception e){

        }
        this.setPath(CREATE_PRIVATE_GROUP);
        this.set("creatorId",app.getUser().getId());
        this.set("partnerId",partnerId);
        this.set("groupName",groupName);
        this.set("longitude", Double.toString(app.getUser().getLocation().getLongitude()));
        this.set("latitude",Double.toString(app.getUser().getLocation().getLatitude()));
        this.set("address",addresses.get(0).toString());
        return this;
    }

    public PlayApi createGroup(String groupName){

        Geocoder geocoder;
        List<Address> addresses = new  ArrayList <Address>();
        geocoder = new Geocoder(app,Locale.getDefault());
        try{
        addresses = geocoder.getFromLocation(app.getUser().getLocation().getLatitude(), app.getUser().getLocation().getLongitude(), 1);
        }catch(Exception e){

        }
        this.setPath(CREATE_GROUP);
        this.set("creatorId",app.getUser().getId());
        this.set("groupName",groupName);
        this.set("longitude", Double.toString(app.getUser().getLocation().getLongitude()));
        this.set("latitude",Double.toString(app.getUser().getLocation().getLatitude()));
        this.set("address",addresses.get(0).toString());
        return this;
    }

    public PlayApi getGroupMembers(String groupId){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(":groupId", groupId);
        this.setPath(GET_GROUP_MEMBERS, params);
        return this;
    }

    public PlayApi addMemberToGroup(String groupId, String userId){
        this.setRequestType(RequestType.PUT);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(":groupId",groupId);
        params.put(":userId",app.getUser().getId());
        this.setPath(ADD_USER_TO_GROUP,params);
        return this;
    }

    public PlayApi joinGroup(String groupId,String userId){
        this.setRequestType(RequestType.PUT);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(":groupId",groupId);
        params.put(":userId",app.getUser().getId());
        this.setPath(JOIN_TO_GROUP,params);
        return this;
    }

    public PlayApi getGroupMessagesByChunk(String groupId,String chunk){
        HashMap<String, String> params = new HashMap<String, String>();
        this.set("groupId",groupId);
        this.set("chunk",chunk);
        this.setRequestType(RequestType.GET);
        this.setPath(GET_GROUP_MESSAGES_BY_CHUNK,params);
        return this;
    }



    public PlayApi getGroupMessages(String userId){
        this.setPath(GET_GROUPS);
        this.set("userId",userId);
        return this;
    }

    public PlayApi inviteGroupMembers(String userId){
        this.setPath(GET_GROUPS);
        this.set("userId",userId);
        return this;
    }

    public PlayApi sendGroupMessage(GroupMessage gm){
        this.setPath(SEND_GROUP_MESSAGE);
        this.set("groupId",gm.getGroupId());
        this.set("userId",gm.getSenderId());
        this.set("userName",gm.getSenderName());
        this.set("text",gm.getText());
        this.set("date",Long.toString(gm.getDate()));
        try{
        this.set("latitude",Double.toString(app.getUser().getLocation().getLatitude()));
            this.set("longitude",Double.toString(app.getUser().getLocation().getLongitude()));
        }catch (Exception e){
            this.set("latitude","0");
            this.set("longitude","0");
        }
        this.set("chunk",Integer.toString(gm.getChunk()));
        this.set("phoneType","Android");
        return this;
    }


    public void processEntity(String apiResponse){
        try{
            if(responseType.equals(Groups.class)){
                JSONArray jsonArray = new JSONArray(apiResponse);
                List<TalkoModel> groups = JsonConverter.convertFromJsonToGroups(jsonArray);
                groupStore = groups;
            func.callback(groups, apiResponse);
            } else if(responseType.equals(Group.class)){
                TalkoModel group = null;
                if (apiResponse.contains("You are already member")){
                        //apiResponse = "{}"; do nothing
                } else {
                    JSONObject jsonObject = new JSONObject(apiResponse);
                    group = JsonConverter.convertFromJsonToGroup(jsonObject);
                }
                func.callback(group, apiResponse);

            } else if(responseType.equals(TalkoUser.class)){
                    JSONObject jsonArray = new JSONObject(apiResponse);
                    List<TalkoModel> members = JsonConverter.convertGetGroupDetailsToUsers(jsonArray);
                    List<TalkoModel> invitees = JsonConverter.convertGetGroupDetailsToInvitees(jsonArray);
                    Map map = new HashMap<String, List<TalkoModel>>();
                    map.put("members", members);
                    map.put("invitees", invitees);
                    func.callback(map, apiResponse);
            }
             else if(responseType.equals(TalkoMessage.class)){
            JSONArray jsonArray = new JSONArray(apiResponse);
            List<TalkoModel> messages = JsonConverter.convertGetGroupMessages(jsonArray);
            func.callback(messages, apiResponse);
        }

        } catch(Exception e){
            throw new RuntimeException(e);
        }
    }
}



