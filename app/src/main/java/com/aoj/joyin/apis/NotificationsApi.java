package com.aoj.joyin.apis;

import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.apis.core.AbstractPlayApi;
import com.aoj.joyin.apis.core.PlayApi;
import com.aoj.joyin.apis.core.RequestType;
import com.aoj.joyin.model.Notification;
import com.aoj.joyin.model.Notifications;
import com.aoj.joyin.model.TalkoModel;
import com.aoj.joyin.utils.JsonConverter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ohad on 2014-07-30.
 */
public class NotificationsApi extends AbstractPlayApi {

    private final String GET_USER_NOTIFICATIONS = "getUserNotifications";
    private final String DISABLE_NOTIFICATION = "disable/:notificationId";
    private final String READ_NOTIFICATION = "read/:notificationId";



    public NotificationsApi(TalkoApp app){
        super(app,Notifications.class);
    }

    public NotificationsApi(TalkoApp app,Class className){
        super(app,className);
    }

    public void configConnector(String userId){
        try{
            JSONObject params = connector.getJsonParams();
            params.put("userId", userId);
        } catch(Exception e){

        }
    }

    public PlayApi getUserNotifications(){
        this.setRequestType(RequestType.POST);
        this.setPath(GET_USER_NOTIFICATIONS);
        return this;
    }

    public PlayApi disableNotification(String notificationId){
        this.setRequestType(RequestType.PUT);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(":notificationId", notificationId);
        this.setPath(DISABLE_NOTIFICATION, params);
        return this;
    }

    public PlayApi readNotification(String notificationId){
        this.setRequestType(RequestType.PUT);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(":notificationId", notificationId);
        this.setPath(READ_NOTIFICATION, params);
        return this;
    }

    public void processEntity(String apiResponse) {

        try {
            if(responseType.equals(Notification.class)){
                JSONObject jsonObject = new JSONObject(apiResponse);
                TalkoModel notification = JsonConverter.convertFromJsonToNotification(jsonObject);
                func.callback(notification, apiResponse);
            } else if(responseType.equals(Notifications.class)){
                JSONArray jsonArray = new JSONArray(apiResponse);
                List<TalkoModel> notifications = JsonConverter.convertFromJsonToNotifications(jsonArray);
                func.callback(notifications, apiResponse);
            }


            } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
