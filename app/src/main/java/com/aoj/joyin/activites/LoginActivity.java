/**
 * Created by ohad on 2013-11-27.
 * updated by Alex.v on 2014.01.14
 */
package com.aoj.joyin.activites;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.aoj.joyin.R;
import com.facebook.Session;
import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.apis.ApiConnector;
import com.aoj.joyin.apis.CallableAbstract;
import com.aoj.joyin.apis.GroupsApi;
import com.aoj.joyin.apis.UserApi;
import com.aoj.joyin.fragments.LoginFragment;
import com.aoj.joyin.model.Group;
import com.aoj.joyin.model.TalkoModel;
import com.aoj.joyin.model.TalkoUser;
import com.aoj.joyin.utils.Consts;
import com.aoj.joyin.utils.TalkoCookieManager;
import com.aoj.joyin.utils.TalkoGCMUtils;
import com.aoj.joyin.utils.TalkoUtils;
import com.aoj.joyin.viewpagerindicator.CirclePageIndicator;
import com.aoj.joyin.viewpagerindicator.PageIndicator;
import com.aoj.joyin.viewpagerindicator.SwipeFragmentAdapter;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class LoginActivity extends FragmentActivity {
    private LoginFragment loginFragment;
    static final String TAG = "Joyin";
    Context context = this;
    SwipeFragmentAdapter mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;
    private String externalGroupId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // add to group from external url
        Intent intent = getIntent();
        String action = intent.getAction();
        Uri uri = intent.getData();
        if (uri!=null) {
            externalGroupId = uri.getQueryParameter("groupId");
        }

        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.osc.talkoloco", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        initGCM();

        CookieHandler.setDefault(new CookieManager(new TalkoCookieManager(this), CookiePolicy.ACCEPT_ALL));

        if (savedInstanceState == null) {
            loginFragment = new LoginFragment();
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, loginFragment).commit();
        } else {
            loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(android.R.id.content);
        }


        TalkoApp app = (TalkoApp) this.getApplication();
        if(app.getUser() != null){
            //setContentView(R.layout.progress_bar);
        }else{
            setContentView(R.layout.login_activity);
            mAdapter = new SwipeFragmentAdapter(getSupportFragmentManager());

            mPager = (ViewPager)findViewById(R.id.intro_pager);
            if (mPager!=null)
                mPager.setAdapter(mAdapter);

            mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
            mIndicator.setViewPager(mPager);
        }


    }

    private void addUserToGroup(String userId, String groupId) {
        final TalkoApp app = (TalkoApp) this.getApplication();
        final LoginActivity that = this;
        GroupsApi groupsApi = new GroupsApi(((TalkoApp) app), Group.class);
        groupsApi.addMemberToGroup(groupId, userId).execute(new CallableAbstract() {
            public void callback(TalkoModel model, String response) {
                Group group = (Group) model;
                if (group == null) {
                    Toast.makeText(that.getApplicationContext(),
                            response,
                            Toast.LENGTH_SHORT).show();
                } else {
                    List<TalkoModel> cacheGroups = (List<TalkoModel>)app.getLocalCache().get("groups");
                    cacheGroups.add(group);
                    app.getLocalCache().put("groups", cacheGroups);
                    Toast.makeText(that.getApplicationContext(),
                            "You joined " + group.getName(),
                            Toast.LENGTH_SHORT).show();
                }
                navigateToIFollow();
            }
        });
    }

    private void initGCM(){
        TalkoUtils talkoUtils = new TalkoUtils();
        TalkoGCMUtils talkoGCMUtils = new TalkoGCMUtils(this);
        // Check device for Play Services APK. If check succeeds, proceed with GCM registration.
        if (talkoUtils.checkPlayServices(this)) {
            talkoGCMUtils.initGCM(this);
        } else {
            Log.i(Consts.TAG, "No valid Google Play Services APK found.");
        }

    }

    /**
     * Login async action that is triggered by facebook sdk.
     * @param session
     */
    public void doLogin(Session session) {

        TalkoApp app = (TalkoApp) this.getApplication();
        app.setSession(session);
        ApiConnector apiConnector = app.getApiConnector();
        UserApi userApis = new UserApi(app);

        TalkoGCMUtils talkoGCMUtils = new TalkoGCMUtils(this);

        String deviceToken = talkoGCMUtils.getRegistrationId();

        userApis.doLogin(session.getAccessToken(), deviceToken, "Android").execute(new CallableAbstract() {
            public void callback(TalkoModel model, String response) {
                afterLoginCallBack(model, response);
            }
        });


    }

    public void logOff(){
       //View loaderView =  findViewById(R.layout.progress_bar);
       //loaderView.setVisibility(View.INVISIBLE);
    }

    /**
     * Handle after login view changes.
     * @param model
     * @param response
     */
    public void afterLoginCallBack(TalkoModel model,String response){
        TalkoUser user =  (TalkoUser)model;
        ((TalkoApp) this.getApplication()).setUser(user);
        if (externalGroupId!=null) {
            addUserToGroup(user.getId(), externalGroupId);
        } else {
            navigateToIFollow();
        }
    }

   @Override
    public void  onNewIntent (Intent intent){
      setIntent(intent);
    }
    /**
     * Navigate to post login view.
     */
    public void navigateToIFollow(){
        Intent intent = new Intent(getApplicationContext(), AppTabContainerActivity.class);
         Intent intentSelf = getIntent();
        if(intentSelf.getExtras() != null &&  intentSelf.getExtras().get("show.group") != null){
           intent.putExtra("show.group",intentSelf.getExtras().get("show.group").toString());
        }
        startActivity(intent);
        this.finish();
    }
}