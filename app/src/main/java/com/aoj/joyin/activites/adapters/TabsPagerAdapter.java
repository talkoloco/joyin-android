package com.aoj.joyin.activites.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.aoj.joyin.fragments.ChatFragment;
import com.aoj.joyin.fragments.ChatListFragment;
import com.aoj.joyin.fragments.GroupManagerFragment;
import com.aoj.joyin.fragments.MyPlacesFragment;
import com.aoj.joyin.fragments.NearbyFragment;

public class TabsPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {

    protected MyPlacesFragment myPlaces = new MyPlacesFragment();
    public TabsPagerAdapter(FragmentManager fm) {
            super(fm);
    }

    private MyPlacesFragment pf = null ;

    @Override
    public Fragment getItem(int index) {
        switch (index) {
            case 0:
                return new MyPlacesFragment();
            case 1:
                return new  GroupManagerFragment();
            case 2:
                return new NearbyFragment();
            case 3:
                return new ChatFragment();
            case 4:
                return new ChatListFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }

    public static String makeFragmentName(int viewId, int index) {
        return "android:switcher:" + viewId + ":" + index;
    }

}