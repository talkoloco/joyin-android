package com.aoj.joyin.activites;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.aoj.joyin.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.aoj.joyin.TalkoApp;
import com.aoj.joyin.activites.adapters.TabsPagerAdapter;
import com.aoj.joyin.apis.CallableAbstract;
import com.aoj.joyin.apis.GroupsApi;
import com.aoj.joyin.apis.UserApi;
import com.aoj.joyin.fragments.ChatFragment;
import com.aoj.joyin.fragments.ChatListFragment;
import com.aoj.joyin.fragments.GroupManagerFragment;
import com.aoj.joyin.fragments.NearbyFragment;
import com.aoj.joyin.hooks.UserGroupsUpdatedHook;
import com.aoj.joyin.model.Group;
import com.aoj.joyin.model.TalkoModel;
import com.aoj.joyin.model.TalkoUser;
import com.aoj.joyin.services.AppLocationService;
import com.aoj.joyin.views.NonSwipeableViewPager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class AppTabContainerActivity extends FragmentActivity  implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        Observer,
        ActionBar.TabListener {
    private final static int  CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private AppTabContainerActivity tabActivity;
    private List<WeakReference<Fragment>> fragList = new ArrayList<WeakReference<Fragment>>();
    private TalkoApp app;
    public int prevFragment = 0;
    public ViewPager getViewPager() {
        return viewPager;
    }

    public AppLocationService getAppLocationService() {
        return appLocationService;
    }

    private AppLocationService appLocationService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (appLocationService==null) {
            appLocationService = new AppLocationService(AppTabContainerActivity.this);
        }
        appLocationService.updateUserLocation();

        app =  (TalkoApp) this.getApplication();

        setContentView(R.layout.activity_main);
        // Initilization
        tabActivity = this;
        viewPager = (NonSwipeableViewPager) findViewById(R.id.pager);
        actionBar = getActionBar();
        fr = getSupportFragmentManager();
        mAdapter = new TabsPagerAdapter(fr);
        viewPager.setAdapter(mAdapter);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);

        // Adding Tabs
        for (String tab_name : tabs) {
            actionBar.addTab(actionBar.newTab().setText(tab_name).setTag("meet-ups")
                    .setTabListener(this));
        }


        if(!showGroupContext()){
            viewPager.setCurrentItem(0, false);
        }

        app.getObservable(UserGroupsUpdatedHook.class).addObserver(this);
    }


    @Override
    public void onAttachFragment (Fragment fragment) {
        fragList.add(new WeakReference(fragment));
    }

    public List<Fragment> getActiveFragments() {
        ArrayList<Fragment> ret = new ArrayList<Fragment>();
        for(WeakReference<Fragment> ref : fragList) {
            Fragment f = ref.get();
            if(f != null) {
                if (f.isVisible()) {
                    ret.add(f);
                }
            }
        }
        return ret;
    }

    public void setViewPager(NonSwipeableViewPager viewPager) {
        this.viewPager = viewPager;
    }

    private NonSwipeableViewPager viewPager;

    public TabsPagerAdapter getmAdapter() {
        return mAdapter;
    }

    public void setmAdapter(TabsPagerAdapter mAdapter) {
        this.mAdapter = mAdapter;
    }

    private TabsPagerAdapter mAdapter;
    private ActionBar actionBar;
    private FragmentManager fr;

    public Group getSelectedGroup() {
        return selectedGroup;
    }

    public void setSelectedGroup(Group selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

    private Group selectedGroup;
    // Tab titles
    private String[] tabs = {TalkoApp.MEETUPS, TalkoApp.CHATS, TalkoApp.TEST};


    public boolean showGroupContext(){
        Intent intent = this.getIntent();
        if(intent.getExtras() != null &&  intent.getExtras().get("show.group") != null){
            viewPager.setCurrentItem(3, false);
            return true;
        }
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

        @Override
    protected void onResume(){


        /*View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);*/
        super.onResume();
        //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }


    public void displayGroupsNearBy(){

                GroupsApi groupsApi = new GroupsApi(((TalkoApp) this.getApplication()));
                final ProgressBar progressBar = (ProgressBar)viewPager.findViewById(R.id.loginProgressBar);
        try{
                progressBar.setVisibility(ProgressBar.VISIBLE);

                groupsApi.getGroupsNearBy().execute(new CallableAbstract() {
                    public void callback(List<TalkoModel> groupList, String response) {

                        afterGroupsNearBy(groupList, response);
                        progressBar.setVisibility(ProgressBar.GONE);
                    }
                });

        }catch (Exception e){
            Toast.makeText(tabActivity.getApplicationContext(),
                    "There was a problem getting your location, please check your device settings.",
                    Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(ProgressBar.GONE);

        }

    }

    public void getGroupDetails(String groupId){
        GroupsApi groupsApi = new GroupsApi(((TalkoApp) this.getApplication()), TalkoUser.class);
        groupsApi.getGroupMembers(groupId).execute(new CallableAbstract() {
            public void callback(Map users, String response){
                afterGetGroupDetails(users, response);
            }
        });
    }

    private void afterGetGroupDetails(Map<String, List<TalkoModel>> users, String response) {
        // TO DO
    }

    public void afterGroupsNearBy(List <TalkoModel> groupList, String response){
        String[] groupNames = new String[groupList.size()+1];
        Integer counter = 0;
        groupNames[counter] = "Create New Group";
        final List<Group> userGroups  = (List<Group>)(List<?>) ((TalkoApp) this.getApplication()).getLocalCache().get("groups");
        final List<Group> groups =  (List<Group>)(List<?>) groupList;


        for(Group i : groups){
            groupNames[++counter] = i.getName();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Join/Add").setItems(groupNames, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0){
                    setSelectedGroup(null);
                    tabActivity.getViewPager().setCurrentItem(1, false);
                    FragmentManager fragmentManger =  tabActivity.getSupportFragmentManager();

                    String nearbyFragmentTag = tabActivity.getmAdapter().makeFragmentName(tabActivity.getViewPager().getId(), 2);
                    NearbyFragment nearbyFragment = (NearbyFragment) fragmentManger.findFragmentByTag(nearbyFragmentTag);
                    nearbyFragment.updateNearbyUsers();

                    String fragmentTag = tabActivity.getmAdapter().makeFragmentName(tabActivity.getViewPager().getId(), 1);
                    GroupManagerFragment groupMangerFragment = (GroupManagerFragment)fragmentManger.findFragmentByTag(fragmentTag);
                    groupMangerFragment.reloadUsersAndTitle();

                }else if(userGroups.contains(groups.get(which-1))){
                    Toast.makeText(tabActivity.getApplicationContext(),
                            "Loading chat...",
                            Toast.LENGTH_SHORT).show();
                    setSelectedGroup(groups.get(which-1));

                    tabActivity.getViewPager().setCurrentItem(3,false);
                    FragmentManager fragmentManger =  tabActivity.getSupportFragmentManager();
                    String fragmentTag = tabActivity.getmAdapter().makeFragmentName(tabActivity.getViewPager().getId(), 3);
                    ChatFragment chatFragment = (ChatFragment)fragmentManger.findFragmentByTag(fragmentTag);

                    chatFragment.getUserGroups();
                    chatFragment.setCachedGroupName(null);
                    Group activeGroup = groups.get(which-1);
                    //chatFragment.setActiveGroup(activeGroup);

                    tabActivity.setSelectedGroup(activeGroup);
                    // placeActivity.getGroupDetails(groups.get(position).getId());

                }else{
                    Group group = groups.get(which-1);
                    GroupsApi groupsApi = new GroupsApi(((TalkoApp) tabActivity.getApplication()), Group.class);
                    groupsApi.joinGroup(group.getId(), ((TalkoApp) tabActivity.getApplication()).getUser().getId()).execute(new CallableAbstract() {
                        public void callback(List<TalkoModel> groupUsers, String response) {
                            Toast.makeText(tabActivity.getApplicationContext(),
                                    "Invite was sent..",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });

                    Toast.makeText(tabActivity.getApplicationContext(),
                            "Request to join sent",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
       // inflater.inflate(R.menu.talko, menu);
      //  inflater.inflate(R.menu.main, menu);
        //menu.addSubMenu(0, 0, 0, "UnFollow").setIcon(R.drawable.talko_icon);
        //appMenu = menu;
        return true;
    }

    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_cart) {
            this.displayGroupsNearBy();
        } else if (id ==R.id.profile){
            this.displayUserProfile();
        }else if (id ==android.R.id.home){
            this.getViewPager().setCurrentItem(0, false);
        }else if (id == 0){
            this.getViewPager().setCurrentItem(0, false);
            GroupsApi.groupStore.remove(GroupsApi.groupStore.indexOf(this.getSelectedGroup()));
            ((UserGroupsUpdatedHook)app.getObservable(UserGroupsUpdatedHook.class)).setGroupsList(GroupsApi.groupStore);
            app.getObservable(UserGroupsUpdatedHook.class).notifyObservers();
            GroupsApi groupsApi = new GroupsApi(((TalkoApp) this.getApplication()));
            groupsApi.removeUserFromGroup(this.getSelectedGroup().getId()).execute(new CallableAbstract() {
                public void callback(List<TalkoModel> groupList, String response) {
                }
            });
            
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    protected void onStart() {
        super.onStart();
        // Connect the client.
       // mLocationClient.connect();
    }
    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        //mLocationClient.disconnect();
        super.onStop();
    }

    public void logout() {
        ((TalkoApp)getApplication()).getSession().closeAndClearTokenInformation();
        Intent intent = new Intent(this, LoginActivity.class);
        ((TalkoApp)this.getApplication()).setUser(null);
        startActivity(intent);
        this.finish();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && ((this.getViewPager().getCurrentItem() == 1 ) || this.getViewPager().getCurrentItem() == 3)) {

            /**
             * stop ongoing movies , animations , voice. (@webview)
             */
            if(fragList.size() > 0){
                if(this.getViewPager().getCurrentItem() == 3){
                    ArrayList<Fragment> fragments = new ArrayList<Fragment>(getActiveFragments());
                    for(Fragment i :fragments){
                        if(i instanceof  ChatFragment){
                            ChatFragment chatFragment = (ChatFragment)i;
                            WebView wv = chatFragment.wv;
                            wv.loadUrl("about:blank");
                            wv = null;
                       }
                    }
                }

                //clear select group on group exit.

            }

            if (TalkoApp.CHATS.equals(app.getSelectedTab())){
                this.getViewPager().setCurrentItem(4, false);
            } else {
                this.getViewPager().setCurrentItem(0, false);
            }

            this.setSelectedGroup(null);
            this.getActionBar().setTitle("");
            this.getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME);

            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("message", "This is my message to be reloaded");
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (appLocationService==null){
            appLocationService = new AppLocationService(AppTabContainerActivity.this);
        }
        appLocationService.updateUserLocation();

        UserApi userApis = new UserApi(app);
        try {
            userApis.saveUserLocation().execute(new CallableAbstract() {
                public void callback(TalkoModel model, String response) {

                }
            });
        } catch (Exception e){
            new AlertDialog.Builder(this)
                    .setTitle("Joyin")
                    .setMessage("Please turn on location services")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {

        }
    }

    public Fragment getFragmentByType(){
        ArrayList<Fragment> fragments = new ArrayList<Fragment>(getActiveFragments());
        for(Fragment i :fragments){
            if(i instanceof  ChatFragment){
                return i;
            }
        }

        return null;
    }

    public void showFragmentByType(){

    }

    @Override
    public void update(Observable observable, Object o) {

    }


    @Override
    public void onTabSelected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {
        String tabName = tab.getText().toString();
        if (tabName.equals(app.getSelectedTab())) {
            return;
        }

        if (tab.getText().equals(TalkoApp.CHATS)) {
            app.setSelectedTab(TalkoApp.CHATS);
            FragmentManager fragmentManger = getSupportFragmentManager();
            this.getViewPager().setCurrentItem(4,false);
            String notificationsFragmentTag = getmAdapter().makeFragmentName(getViewPager().getId(), 4);
            ChatListFragment chatListFragment = (ChatListFragment) fragmentManger.findFragmentByTag(notificationsFragmentTag);
            if (chatListFragment !=null) {
                chatListFragment.updateChats();
            }
        } else {
            app.setSelectedTab(TalkoApp.MEETUPS);
            //FragmentManager fragmentManger = getSupportFragmentManager();
            this.getViewPager().setCurrentItem(0, false);
            //String meetupsFragmentName = getmAdapter().makeFragmentName(getViewPager().getId(), 0);
            //MyPlacesFragment meetupsFragment = (MyPlacesFragment) fragmentManger.findFragmentByTag(meetupsFragmentName);
            // }
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {

    }
}
