package com.aoj.joyin.viewpagerindicator;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SwipeFragmentAdapter extends FragmentPagerAdapter implements IconPageAdapter {
    protected static final String[] TITLES = new String[] { "Sign In", "Community", "It's Safe"};
    protected static final String[] CONTENT = new String[] {"We'll never post anything on your behalf" , "Increase your favorite place engagement", "We respect your privacy!" };
    protected static final String[] PICS = new String[] {"hello" , "love", "smile" };


    private int mCount = TITLES.length;

    public SwipeFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return IntroPartViewFragment.newInstance(TITLES[position % TITLES.length], CONTENT[position % CONTENT.length], PICS[position % PICS.length]);
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Joyin";
        //return SwipeFragmentAdapter.CONTENT[position % CONTENT.length];
    }

    @Override
    public int getIconResId(int index) {
        return 0;
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}