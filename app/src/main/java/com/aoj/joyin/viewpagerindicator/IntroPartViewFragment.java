package com.aoj.joyin.viewpagerindicator;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public final class IntroPartViewFragment extends Fragment {
    private static final String KEY_CONTENT = "IntroPartViewFragment:Content";
    private static final String KEY_TITLE = "IntroPartViewFragment:Titles";
    private static final String KEY_PIC = "IntroPartViewFragment:Pic";



    public static IntroPartViewFragment newInstance(String title, String content, String pic) {
        IntroPartViewFragment fragment = new IntroPartViewFragment();

        fragment.mContent = content;
        fragment.mTitle = title;
        fragment.mPic = pic;


        return fragment;
    }

    private String mContent = "???";
    private String mTitle = "???";
    private String mPic = "???";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_TITLE)) {
            mTitle = savedInstanceState.getString(KEY_TITLE);

        }
        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_PIC)) {
            mPic = savedInstanceState.getString(KEY_PIC);
        }
    }


            @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        TextView contentText = new TextView(getActivity());
        contentText.setGravity(Gravity.CENTER);
        contentText.setText(mContent);
        contentText.setTextSize(5 * getResources().getDisplayMetrics().density);
        contentText.setPadding(20, 20, 20, 20);

        TextView titleText = new TextView(getActivity());
        titleText.setGravity(Gravity.CENTER);
        titleText.setText(mTitle);
        titleText.setTextSize(15 * getResources().getDisplayMetrics().density);
        titleText.setPadding(20, 20, 20, 20);

        //ImageView Setup
        ImageView imageView = new ImageView(getActivity());
        //setting image resource
        imageView.setImageResource(getImageId(getActivity(), "intro_" + mPic));
                //setting image position
        imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        int orientation = getResources().getConfiguration().orientation;

        if (orientation== Configuration.ORIENTATION_PORTRAIT){
            imageView.getLayoutParams().height = 600;
        } else {
            imageView.getLayoutParams().height = 300;

        }

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setGravity(Gravity.CENTER);
        layout.addView(imageView);
        layout.addView(titleText);
        layout.addView(contentText);

        return layout;
    }

    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
        outState.putString(KEY_TITLE, mTitle);
        outState.putString(KEY_PIC, mPic);


    }
}
